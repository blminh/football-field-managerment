﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class NhanVienDTO
    {
        public string MaNV { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public Nullable<bool> GioiTinh { get; set; }
        public string SoDienThoai { get; set; }
        public string Email { get; set; }
        public string MatKhau { get; set; }
        public System.DateTime NgayBatDau { get; set; }
        public byte LoaiNV { get; set; }
        public decimal Luong { get; set; }
        public bool TrangThai { get; set; }
    }
}
