﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LoaiDVDTO    {
        public string LoaiDV { get; set; }
        public string TenLoaiDV { get; set; }
    }
}
