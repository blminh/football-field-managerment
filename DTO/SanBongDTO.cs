﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SanBongDTO
    {
        public string MaSanBong { get; set; }
        public byte LoaiSanBong { get; set; }
        public string KhuVuc { get; set; }
        public string TenSanBong { get; set; }
        public byte TrangThai { get; set; }
    }
}
