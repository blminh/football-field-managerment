﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ChiTietDatSanDTO
    {
        public string MaHD { get; set; }
        public string MaSanBong { get; set; }
        public decimal GiaTien { get; set; }
        public string Hoten { get; set; }
        public string SoDienThoai { get; set; }
        public System.DateTime GioBatDau { get; set; }
        public System.DateTime GioKetThuc { get; set; }
    }
}
