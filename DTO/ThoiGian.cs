﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ThoiGian
    {
        public string MaSan { get; set; }
        public int ThoiGianThue { get; set; }
        public DateTime ThoiGianVao { get; set; }
        public DateTime ThoiGianRa { get; set; }

        public static implicit operator List<object>(ThoiGian v)
        {
            throw new NotImplementedException();
        }
    }
}
