﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PhieuDatSanDTO
    {
        public int Stt { get; set; }
        public string SoDienThoai { get; set; }
        public string TenKH { get; set; }
        public decimal GiaTien { get; set; }
        public string MaSan { get; set; }
        public System.DateTime GioBatDau { get; set; }
        public System.DateTime GioKetThuc { get; set; }
        public int ThoiGianThue { get; set; }
    }
}
