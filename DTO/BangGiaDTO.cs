﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class BangGiaDTO
    {
        public byte LoaiSanBong { get; set; }
        public decimal Gia { get; set; }
    }
}
