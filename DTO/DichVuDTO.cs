﻿using System;
using DevExpress.Xpo;

namespace DTO
{

    public class DichVuDTO
    {
        public string MaDV { get; set; }
        public string LoaiDV { get; set; }
        public string TenDV { get; set; }
        public decimal GiaMua { get; set; }
        public Nullable<decimal> GiaBan { get; set; }
        public Nullable<byte> TrangThai { get; set; }
    }

}