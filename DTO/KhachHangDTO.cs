﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class KhachHangDTO
    {
        public string MaKH { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public string SoDienThoai { get; set; }
        public byte LoaiKH { get; set; }
        public bool TrangThai { get; set; }
        public string Email { get; set; }
        public Nullable<bool> GioiTinh { get; set; }
        public double DIEMTICHLUY { get; set; }
    }
}
