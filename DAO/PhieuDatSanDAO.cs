﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public class PhieuDatSanDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();
        public List<PhieuDatSanDTO> dsDatSan()
        {
            return db.PHIEUDATSANs.Where(u=>u.TRANGTHAI == 1).Select(s => new PhieuDatSanDTO
            {
                Stt = s.STT,
                SoDienThoai = s.SODIENTHOAI,
                TenKH = s.TENKH,
                MaSan = s.MASAN,
                GiaTien = s.GIATIEN,
                GioBatDau = s.GIOBATDAU,
                GioKetThuc = s.GIOKETTHUC
            }).ToList();
        }

        public List<PhieuDatSanDTO> dsPhieuDatSan(string sdt, DateTime dt)
        {
            List<PhieuDatSanDTO> result =  db.PHIEUDATSANs
                    .Where(u => u.TRANGTHAI == 1 
                            && u.SODIENTHOAI == sdt
                            )
                    .Select(s=>new PhieuDatSanDTO{
                        SoDienThoai = s.SODIENTHOAI,
                        TenKH = s.TENKH,
                        MaSan = s.MASAN,
                        GiaTien = s.GIATIEN,
                        GioBatDau = s.GIOBATDAU,
                        GioKetThuc = s.GIOKETTHUC,
                        ThoiGianThue = s.GIOKETTHUC.Hour * 60 + s.GIOKETTHUC.Minute - (s.GIOBATDAU.Hour * 60 + s.GIOBATDAU.Minute),
                    })
                    .ToList();

            return result;
        }

        public bool themPhieu(List<PhieuDatSanDTO> lst)
        {
            try
            {
                foreach (PhieuDatSanDTO item in lst)
                {
                    PHIEUDATSAN pds = new PHIEUDATSAN
                    {
                        SODIENTHOAI = item.SoDienThoai,
                        TENKH = item.TenKH,
                        MASAN = item.MaSan,
                        GIOBATDAU = item.GioBatDau,
                        GIOKETTHUC = item.GioKetThuc,
                        GIATIEN = item.GiaTien,
                        TRANGTHAI = 1
                    };
                    db.PHIEUDATSANs.Add(pds);
                }
                db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public void ThanhToanPhieu(PhieuDatSanDTO pdsDTO)
        {
            PHIEUDATSAN pds = (db.PHIEUDATSANs
                .Where(u => u.SODIENTHOAI == pdsDTO.SoDienThoai && u.MASAN == pdsDTO.MaSan && u.TRANGTHAI == 1)
                .Select(s => s)).ToList()[0];
            pds.TRANGTHAI = 2;
            db.SaveChanges();
        }

        public void HuyPhieu(PhieuDatSanDTO pdsDTO)
        {
            PHIEUDATSAN pds = (db.PHIEUDATSANs
                .Where(u => u.SODIENTHOAI == pdsDTO.SoDienThoai && u.MASAN == pdsDTO.MaSan && u.TRANGTHAI == 1)
                .Select(s => s)).ToList()[0];
            pds.TRANGTHAI = 0;
            db.SaveChanges();
        }
    }
}
