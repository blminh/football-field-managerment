﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;


namespace DAO
{
    public class ChiTietDatSanDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();
        public List<ChiTietDatSanDTO> dsChiTiet()
        {
            var result = from ct in db.CHITIETDATSANs select new ChiTietDatSanDTO
            {
                MaHD = ct.MAHD,
                MaSanBong = ct.MASANBONG,
                GioBatDau = ct.GIOBATDAU,
                GioKetThuc = ct.GIOKETTHUC,
                Hoten=ct.TENKH,
                GiaTien=ct.GIATIEN,
                SoDienThoai=ct.SODIENTHOAI
                
            };
            return result.ToList();
        }

        public bool ThemChiTietHoaDon(ChiTietDatSanDTO ctdsDTO)
        {
            try
            {
                db.CHITIETDATSANs.Add(new CHITIETDATSAN
                {
                    MAHD = ctdsDTO.MaHD,
                    MASANBONG = ctdsDTO.MaSanBong,
                    GIATIEN = ctdsDTO.GiaTien,
                    GIOBATDAU = ctdsDTO.GioBatDau,
                    GIOKETTHUC = ctdsDTO.GioKetThuc,
                    TENKH = ctdsDTO.Hoten,
                    SODIENTHOAI = ctdsDTO.SoDienThoai
                });
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
    }
}
