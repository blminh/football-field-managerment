﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;

namespace DAO
{
    public class HoaDonDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();
        public List<HoaDonDTO> layDSHoaDon()
        {
            return db.HOADONs.Select(s => new HoaDonDTO
            {
                MaHD = s.MAHD,
                MaNV = s.MANV,
                MaKH = s.MAKH,
                NgayLap = s.NGAYLAP,
                TrangThai = s.TRANGTHAI
            }).ToList();
        }

        public bool ThemHoaDon(HoaDonDTO hdDTO)
        {
            try
            {
                HOADON hd = new HOADON();

                hd.MAHD = hdDTO.MaHD;
                hd.MANV = hdDTO.MaNV;
                hd.MAKH = hdDTO.MaKH;
                hd.NGAYLAP = hdDTO.NgayLap;
                hd.TRANGTHAI = true;

                db.HOADONs.Add(hd);
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
                throw;
            }
        }
    }
}
