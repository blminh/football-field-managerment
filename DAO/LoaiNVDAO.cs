﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public class LoaiNVDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();
        public List<LoaiNVDTO> layDSLoaiNV()
        {
            var result = from lnv in db.LOAINHANVIENs select lnv;
            List<LoaiNVDTO> lstLNV = new List<LoaiNVDTO>();
            foreach (var item in result)
            {
                LoaiNVDTO lnv = new LoaiNVDTO
                {
                    LoaiNV = item.LOAINV,
                    TenLoaiNV = item.TENLOAINV
                };
                lstLNV.Add(lnv);
            }

            return lstLNV;
        }
    }
}
