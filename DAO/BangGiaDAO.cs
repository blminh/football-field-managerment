﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public class BangGiaDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();

        public List<BangGiaDTO> layDSBangGia()
        {

            List<BangGiaDTO> result = db.BANGGIAs.Select(bg => new BangGiaDTO
            {
                LoaiSanBong = bg.LOAISANBONG,
                Gia = bg.GIA,

            }).ToList();

            return result;
        }
        public List<BangGiaDTO> layDSSanBongTheoLoai(byte loaisb)
        {
            List<BangGiaDTO> sanbong = new List<BangGiaDTO>();
            BANGGIA result = db.BANGGIAs.SingleOrDefault(u => u.LOAISANBONG == loaisb);
            BangGiaDTO sb = new BangGiaDTO();

            sb.LoaiSanBong = result.LOAISANBONG;
            sb.Gia = result.GIA;
            sanbong.Add(sb);
            return sanbong;
        }
    }
}
