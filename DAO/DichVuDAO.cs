﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Data.Helpers;
using DTO;

namespace DAO
{
    public class DichVuDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();

        public List<DichVuDTO> dsChiTiet()
        {

            List<DichVuDTO> result = db.DichVus.Select(dv => new DichVuDTO
            {
                LoaiDV=dv.LoaiDV,
                TenDV = dv.TenDV,
                GiaBan = (int)dv.GiaBan,
            }).ToList();

            return result;
        }

        public List<DichVuDTO> dsChiTiet1(string maLoai)
        {

            List<DichVuDTO> result = db.DichVus.Where(x=>x.LoaiDV==maLoai).Select(dv => new DichVuDTO
            {
                LoaiDV = dv.LoaiDV,
                TenDV = dv.TenDV,
                GiaBan = (int)dv.GiaBan,
            }).ToList();

            return result;
        }
        

    }
}
