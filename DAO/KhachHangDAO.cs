﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;

namespace DAO
{
    public class KhachHangDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();
        public List<KhachHangDTO> loadDSKhachHang()
        {
            return db.KHACHHANGs.Where(u=>u.TRANGTHAI == true).Select(item => new KhachHangDTO
            {
                MaKH = item.MAKH,
                Ho = item.HO,
                Ten = item.TEN,
                SoDienThoai = item.SODIENTHOAI,
                Email = item.EMAIL,
                GioiTinh = item.GIOITINH,
                LoaiKH = item.LOAIKH,
                TrangThai = item.TRANGTHAI,
                DIEMTICHLUY = item.DIEMTICHLUY
            }).ToList();
        }

        private string phatSinhMaKH()
        {
            int sl = db.KHACHHANGs.Count();
            string ma = "KH0"+(sl+1);
            return ma;
        }

        public bool ThemKhachHang(KhachHangDTO khDTO)
        {
            try
            {
                KHACHHANG kh = new KHACHHANG
                {
                    MAKH = phatSinhMaKH(),
                    HO = khDTO.Ho,
                    TEN = khDTO.Ten,
                    SODIENTHOAI = khDTO.SoDienThoai,
                    EMAIL = khDTO.Email,
                    GIOITINH = khDTO.GioiTinh,
                    TRANGTHAI = true,
                    LOAIKH = 1,
                    DIEMTICHLUY = 0
                };
                foreach (string makh in db.KHACHHANGs.Select(u=>u.MAKH))
                {
                    if(makh == kh.MAKH)
                    {
                        return false;
                    }
                    else
                    {
                        db.KHACHHANGs.Add(kh);
                    }
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                throw;
            }
        }

        public void ThangHangKhachHang(KhachHangDTO khDTO)
        {
            KHACHHANG kh = db.KHACHHANGs.Where(u => u.MAKH == khDTO.MaKH).Select(s => s).ToList()[0];
            kh.DIEMTICHLUY = khDTO.DIEMTICHLUY;
            kh.LOAIKH = khDTO.LoaiKH;
            db.SaveChanges();
        }

        public bool SuaKhachHang(KhachHangDTO khDTO)
        {
            try
            {
                KHACHHANG kh = db.KHACHHANGs.Where(u => u.MAKH == khDTO.MaKH).Select(s => s).ToList()[0];

                kh.MAKH = khDTO.MaKH;
                kh.HO = khDTO.Ho;
                kh.TEN = khDTO.Ten;
                kh.SODIENTHOAI = khDTO.SoDienThoai;
                kh.EMAIL = khDTO.Email;
                kh.LOAIKH = khDTO.LoaiKH;
                kh.GIOITINH = khDTO.GioiTinh;
                kh.TRANGTHAI = true;
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                throw;
            }
        }
        public KhachHangDTO TimKh(string maKH)
        {
            KHACHHANG result = db.KHACHHANGs.SingleOrDefault(u => u.MAKH == maKH );

            KhachHangDTO kh = new KhachHangDTO();
            kh.MaKH = result.MAKH;
            kh.Ho = result.HO;
            kh.Ten = result.TEN;
            kh.Email = result.EMAIL;
            kh.SoDienThoai = result.SODIENTHOAI;
            kh.TrangThai = true;
            kh.LoaiKH = result.LOAIKH;
            kh.GioiTinh = result.GIOITINH;
            kh.DIEMTICHLUY = result.DIEMTICHLUY;
            return kh;
        }
        public bool XoaKhachHang(string makh)
        {
            try
            {
                KHACHHANG kh = db.KHACHHANGs.Where(u => u.MAKH == makh).Select(s => s).ToList()[0];   
                kh.TRANGTHAI = false;
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
                throw;
            }
        }
    }
}
