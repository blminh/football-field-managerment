﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;

namespace DAO
{
    public class NhanVienDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();
        NhanVienDTO nv = new NhanVienDTO();
        public List<NhanVienDTO> loadDSNhanVien()
        {
            var result = from nv in db.NHANVIENs
                         where nv.TRANGTHAI == true && nv.MANV != "Admin"
                         select nv;
            List<NhanVienDTO> lstNV = new List<NhanVienDTO>();
            foreach (var item in result)
            {
                NhanVienDTO nv = new NhanVienDTO
                {
                    MaNV = item.MANV,
                    Ho = item.HO,
                    Ten = item.TEN,
                    SoDienThoai = item.SODIENTHOAI,
                    Email = item.EMAIL,
                    MatKhau = item.MATKHAU,
                    NgayBatDau = item.NGAYBATDAU,
                    LoaiNV = item.LOAINV,
                    GioiTinh = item.GIOITINH,
                    Luong = item.LUONG,
                    TrangThai = item.TRANGTHAI
                };
                lstNV.Add(nv);
            }

            return lstNV;
        }

        public string MaHoa(string pass)
        {
            
            string convertMD5 = "";
            byte[] bytes = Encoding.UTF8.GetBytes(pass);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            bytes = md5.ComputeHash(bytes);
            for (int i = 0; i < bytes.Length; i++)
            {
                convertMD5 = convertMD5 + bytes[i].ToString("x2");
            }
            return convertMD5;
        }

        private string phatSinhMaNV()
        {
            int sl = db.NHANVIENs.Count();
            string ma = "NV0" + (sl + 1);
            return ma;
        }

        public NhanVienDTO Timnv(string maNV)
        {
            NHANVIEN result = db.NHANVIENs.SingleOrDefault(u => u.MANV == maNV);
            nv.MaNV = result.MANV;
            nv.Ho = result.HO;
            nv.Ten = result.TEN;
            nv.Email = result.EMAIL;
            nv.SoDienThoai = result.SODIENTHOAI;
            nv.TrangThai = true;
            nv.Luong = result.LUONG;
            nv.GioiTinh = result.GIOITINH;
            nv.MatKhau = result.MATKHAU;
            nv.LoaiNV = result.LOAINV;
            return nv;
        }

        public bool themNhanVien(NhanVienDTO nvDTO)
        {
            try
            {
                NHANVIEN nv = new NHANVIEN();
                nv.MANV = phatSinhMaNV();
                nv.HO = nvDTO.Ho;
                nv.TEN = nvDTO.Ten;
                nv.SODIENTHOAI = nvDTO.SoDienThoai;
                nv.EMAIL = nvDTO.Email;
                nv.GIOITINH = nvDTO.GioiTinh;
                nv.NGAYBATDAU = nvDTO.NgayBatDau;
                nv.MATKHAU = MaHoa(nvDTO.MatKhau);
                nv.LUONG = nvDTO.Luong;
                nv.LOAINV = nvDTO.LoaiNV;
                nv.TRANGTHAI = true;
                foreach (string manv in db.NHANVIENs.Select(u => u.MANV))
                {
                    if (manv == nv.MANV)
                    {
                        return false;
                    }
                    else
                    {
                        db.NHANVIENs.Add(nv);
                    }
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                throw;
            }
        }

        public bool suaNhanVien(NhanVienDTO nvDTO)
        {
            try
            {
                NHANVIEN nv = db.NHANVIENs.Where(u => u.MANV == nvDTO.MaNV).Select(s => s).ToList()[0];
                nv.HO = nvDTO.Ho;
                nv.TEN = nvDTO.Ten;
                nv.SODIENTHOAI = nvDTO.SoDienThoai;
                nv.EMAIL = nvDTO.Email;
                nv.GIOITINH = nvDTO.GioiTinh;
                nv.NGAYBATDAU = nvDTO.NgayBatDau;
                nv.MATKHAU = MaHoa(nvDTO.MatKhau);
                nv.LUONG = nvDTO.Luong;
                nv.LOAINV = nvDTO.LoaiNV;
                nv.TRANGTHAI = true;
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
                throw;
            }
        }

        public bool xoaNhanVien(string maNV)
        {
            try
            {
                NHANVIEN nv = db.NHANVIENs.Where(u => u.MANV == maNV).Select(s => s).ToList()[0];
                if(nv.LOAINV == 0)
                {
                    nv.TRANGTHAI = false;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
                throw;
            }
        }
        public List<NhanVienDTO> layDSTaiKhoan()
        {
            List<NhanVienDTO> result = db.NHANVIENs
                                        .Where(u => u.TRANGTHAI == true)
                                        .Select(nv => new NhanVienDTO
                                        {
                                            MaNV = nv.MANV
                                        }).ToList();

            return result;
        }
    }
}
