﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public class SanBongDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();

        public List<SanBongDTO> layDSSanBong()
        {

            List<SanBongDTO> result = db.SANBONGs.Where(u=>u.TRANGTHAI != 0).Select(sb=>new SanBongDTO
            {
                MaSanBong = sb.MASANBONG,
                LoaiSanBong = sb.LOAISANBONG,
                TenSanBong = sb.TENSANBONG,
                KhuVuc = sb.KHUVUC,
                TrangThai = sb.TRANGTHAI
            }).ToList();
            
            return result;
        }

        public bool ThemSanBong(SanBongDTO sbDTO)
        {
            try
            {
                SANBONG sb = new SANBONG
                {
                    MASANBONG = sbDTO.MaSanBong,
                    LOAISANBONG = sbDTO.LoaiSanBong,
                    KHUVUC = sbDTO.KhuVuc,
                    TENSANBONG = sbDTO.TenSanBong,
                    TRANGTHAI = 1
                };

                db.SANBONGs.Add(sb);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool SuaSanBong(string masan)
        {
            try
            {
                SANBONG sb = (db.SANBONGs
                .Where(u=>u.MASANBONG == masan)
                .Select(s=>s)).ToList()[0];
                if (sb.TRANGTHAI == 1)
                    sb.TRANGTHAI = 2;
                else if(sb.TRANGTHAI == 2) sb.TRANGTHAI = 1;
                db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool XoaSanBong(string masan)
        {
            try
            {
                SANBONG sb = (db.SANBONGs
                .Where(u=>u.MASANBONG == masan)
                .Select(s=>s)).ToList()[0];
                sb.TRANGTHAI = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
