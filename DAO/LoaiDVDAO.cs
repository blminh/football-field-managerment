﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public class LoaiDVDAO
    {
        QuanLySanBongEntities db = new QuanLySanBongEntities();

        public List<LoaiDVDTO> layDSloaiDV()
        {

            List<LoaiDVDTO> result = db.LoaiDVs.Select(bg=>new LoaiDVDTO
            {
                TenLoaiDV = bg.TenLoaiDV,
                LoaiDV = bg.LoaiDV1
            }).ToList();

            return result;
        }

      
    }
}
