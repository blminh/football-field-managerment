﻿namespace QLSB
{
    partial class frmDSLichDatSan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDSLichDatSan));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gvDSDatSan = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoDienThoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateVao = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colTimeT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateRa = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colGiaTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnThanhToan = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colHuy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnHuy = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.chkChon = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnTimPhieu = new DevExpress.XtraEditors.SimpleButton();
            this.radNhieu = new System.Windows.Forms.RadioButton();
            this.radDon = new System.Windows.Forms.RadioButton();
            this.txtLamMoi = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gvDSDatSan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateVao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateVao.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateRa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateRa.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnThanhToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHuy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gvDSDatSan
            // 
            this.gvDSDatSan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvDSDatSan.Location = new System.Drawing.Point(0, 93);
            this.gvDSDatSan.MainView = this.gridView1;
            this.gvDSDatSan.Name = "gvDSDatSan";
            this.gvDSDatSan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnHuy,
            this.btnThanhToan,
            this.chkChon,
            this.dateVao,
            this.dateRa});
            this.gvDSDatSan.Size = new System.Drawing.Size(800, 357);
            this.gvDSDatSan.TabIndex = 0;
            this.gvDSDatSan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKhachHang,
            this.colSoDienThoai,
            this.colSan,
            this.colTimeF,
            this.colTimeT,
            this.colGiaTien,
            this.colThanhToan,
            this.colHuy});
            this.gridView1.GridControl = this.gvDSDatSan;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            // 
            // colKhachHang
            // 
            this.colKhachHang.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colKhachHang.AppearanceCell.Options.UseFont = true;
            this.colKhachHang.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colKhachHang.AppearanceHeader.Options.UseFont = true;
            this.colKhachHang.Caption = "Tên khách hàng";
            this.colKhachHang.FieldName = "TenKH";
            this.colKhachHang.Name = "colKhachHang";
            this.colKhachHang.Visible = true;
            this.colKhachHang.VisibleIndex = 0;
            // 
            // colSoDienThoai
            // 
            this.colSoDienThoai.Caption = "Số điện thoại";
            this.colSoDienThoai.FieldName = "SoDienThoai";
            this.colSoDienThoai.Name = "colSoDienThoai";
            this.colSoDienThoai.Visible = true;
            this.colSoDienThoai.VisibleIndex = 1;
            this.colSoDienThoai.Width = 78;
            // 
            // colSan
            // 
            this.colSan.Caption = "San";
            this.colSan.FieldName = "MaSan";
            this.colSan.Name = "colSan";
            this.colSan.Visible = true;
            this.colSan.VisibleIndex = 2;
            // 
            // colTimeF
            // 
            this.colTimeF.Caption = "Vào";
            this.colTimeF.ColumnEdit = this.dateVao;
            this.colTimeF.FieldName = "GioBatDau";
            this.colTimeF.Name = "colTimeF";
            this.colTimeF.Visible = true;
            this.colTimeF.VisibleIndex = 3;
            this.colTimeF.Width = 103;
            // 
            // dateVao
            // 
            this.dateVao.AutoHeight = false;
            this.dateVao.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateVao.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateVao.DisplayFormat.FormatString = "g";
            this.dateVao.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateVao.EditFormat.FormatString = "g";
            this.dateVao.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateVao.Mask.EditMask = "g";
            this.dateVao.Name = "dateVao";
            // 
            // colTimeT
            // 
            this.colTimeT.Caption = "Ra";
            this.colTimeT.ColumnEdit = this.dateRa;
            this.colTimeT.FieldName = "GioKetThuc";
            this.colTimeT.Name = "colTimeT";
            this.colTimeT.Visible = true;
            this.colTimeT.VisibleIndex = 4;
            this.colTimeT.Width = 103;
            // 
            // dateRa
            // 
            this.dateRa.AutoHeight = false;
            this.dateRa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateRa.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateRa.DisplayFormat.FormatString = "g";
            this.dateRa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateRa.EditFormat.FormatString = "g";
            this.dateRa.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateRa.Name = "dateRa";
            // 
            // colGiaTien
            // 
            this.colGiaTien.Caption = "Giá tiền";
            this.colGiaTien.DisplayFormat.FormatString = "#.#";
            this.colGiaTien.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGiaTien.FieldName = "GiaTien";
            this.colGiaTien.Name = "colGiaTien";
            this.colGiaTien.Visible = true;
            this.colGiaTien.VisibleIndex = 5;
            // 
            // colThanhToan
            // 
            this.colThanhToan.Caption = "Thanh toán";
            this.colThanhToan.ColumnEdit = this.btnThanhToan;
            this.colThanhToan.Name = "colThanhToan";
            this.colThanhToan.Visible = true;
            this.colThanhToan.VisibleIndex = 6;
            this.colThanhToan.Width = 103;
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.AutoHeight = false;
            editorButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions5.Image")));
            this.btnThanhToan.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnThanhToan.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnThanhToan_ButtonClick);
            // 
            // colHuy
            // 
            this.colHuy.Caption = "Hủy đặt sân";
            this.colHuy.ColumnEdit = this.btnHuy;
            this.colHuy.Name = "colHuy";
            this.colHuy.Visible = true;
            this.colHuy.VisibleIndex = 7;
            this.colHuy.Width = 113;
            // 
            // btnHuy
            // 
            this.btnHuy.AutoHeight = false;
            editorButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions6.Image")));
            this.btnHuy.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnHuy.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnHuy_ButtonClick);
            // 
            // chkChon
            // 
            this.chkChon.AutoHeight = false;
            this.chkChon.Name = "chkChon";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.txtLamMoi);
            this.groupControl1.Controls.Add(this.btnTimPhieu);
            this.groupControl1.Controls.Add(this.radNhieu);
            this.groupControl1.Controls.Add(this.radDon);
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(800, 74);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Chức năng";
            // 
            // btnTimPhieu
            // 
            this.btnTimPhieu.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimPhieu.Appearance.Options.UseFont = true;
            this.btnTimPhieu.Enabled = false;
            this.btnTimPhieu.Location = new System.Drawing.Point(527, 25);
            this.btnTimPhieu.Name = "btnTimPhieu";
            this.btnTimPhieu.Size = new System.Drawing.Size(112, 44);
            this.btnTimPhieu.TabIndex = 2;
            this.btnTimPhieu.Text = "Tìm phiếu";
            this.btnTimPhieu.Click += new System.EventHandler(this.btnTimPhieu_Click);
            // 
            // radNhieu
            // 
            this.radNhieu.AutoSize = true;
            this.radNhieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNhieu.Location = new System.Drawing.Point(270, 34);
            this.radNhieu.Name = "radNhieu";
            this.radNhieu.Size = new System.Drawing.Size(219, 28);
            this.radNhieu.TabIndex = 1;
            this.radNhieu.Text = "Thanh toán theo phiếu";
            this.radNhieu.UseVisualStyleBackColor = true;
            this.radNhieu.CheckedChanged += new System.EventHandler(this.radNhieu_CheckedChanged);
            // 
            // radDon
            // 
            this.radDon.AutoSize = true;
            this.radDon.Checked = true;
            this.radDon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDon.Location = new System.Drawing.Point(78, 34);
            this.radDon.Name = "radDon";
            this.radDon.Size = new System.Drawing.Size(163, 28);
            this.radDon.TabIndex = 0;
            this.radDon.TabStop = true;
            this.radDon.Text = "Thanh toán đơn";
            this.radDon.UseVisualStyleBackColor = true;
            this.radDon.CheckedChanged += new System.EventHandler(this.radDon_CheckedChanged);
            // 
            // txtLamMoi
            // 
            this.txtLamMoi.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLamMoi.Appearance.Options.UseFont = true;
            this.txtLamMoi.Location = new System.Drawing.Point(676, 25);
            this.txtLamMoi.Name = "txtLamMoi";
            this.txtLamMoi.Size = new System.Drawing.Size(112, 44);
            this.txtLamMoi.TabIndex = 3;
            this.txtLamMoi.Text = "Làm mới";
            this.txtLamMoi.Click += new System.EventHandler(this.txtLamMoi_Click);
            // 
            // frmDSLichDatSan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.gvDSDatSan);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDSLichDatSan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Danh sách đặt sân";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.gvDSDatSan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateVao.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateVao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateRa.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateRa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnThanhToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHuy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gvDSDatSan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn colSoDienThoai;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeT;
        private DevExpress.XtraGrid.Columns.GridColumn colGiaTien;
        private DevExpress.XtraGrid.Columns.GridColumn colThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn colHuy;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnHuy;
        private DevExpress.XtraGrid.Columns.GridColumn colSan;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeF;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnThanhToan;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkChon;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dateVao;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dateRa;
        private System.Windows.Forms.RadioButton radNhieu;
        private System.Windows.Forms.RadioButton radDon;
        private DevExpress.XtraEditors.SimpleButton btnTimPhieu;
        private DevExpress.XtraEditors.SimpleButton txtLamMoi;
    }
}