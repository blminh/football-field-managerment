﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;

namespace QLSB
{
    public partial class frmTimPhieu : DevExpress.XtraEditors.XtraForm
    {
        public delegate void TruyenPhieu(List<PhieuDatSanDTO> lst, string sdt);
        public event TruyenPhieu send;
        public frmTimPhieu()
        {
            InitializeComponent();
        }

        PhieuDatSanBUS pdsDTO = new PhieuDatSanBUS();
        private void btnTimPhieu_Click(object sender, EventArgs e)
        {
            if (txtSDT.Text == "")
                ThongBao.chuanhap();
            else
            if (send != null)
            {
                DateTime dt = new DateTime(dateNgay.DateTime.Year, dateNgay.DateTime.Month, dateNgay.DateTime.Day, timeGio.Time.Hour, timeGio.Time.Minute, timeGio.Time.Second);
                List<PhieuDatSanDTO> lstpds = new List<PhieuDatSanDTO>();
                lstpds = pdsDTO.dsPhieuDatSan(txtSDT.Text, dt);
                send(lstpds, txtSDT.Text);
                this.Close();
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmTimPhieu_Load(object sender, EventArgs e)
        {
            dateNgay.DateTime = DateTime.Now;
            timeGio.Time = DateTime.Now;
        }
    }
}
