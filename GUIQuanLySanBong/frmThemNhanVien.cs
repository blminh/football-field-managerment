﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;
namespace QLSB
{
    public partial class frmThemNhanVien : DevExpress.XtraEditors.XtraForm
    {
        public frmThemNhanVien()
        {
            InitializeComponent();
        }

        NhanVienBUS nvBUS = new NhanVienBUS();
        private void btnTaoTaiKhoan_Click(object sender, EventArgs e)
        {
            if (txtHo.Text != "" && txtTen.Text != "" && txtSDT.Text != "" && txtEmail.Text != "" && txtMatKhau.Text != "" && txtLuong.Text != "")
            {
                NhanVienDTO nvDTO = new NhanVienDTO
                {
                    Ho = txtHo.Text,
                    Ten = txtTen.Text,
                    SoDienThoai = txtSDT.Text,
                    Email = txtEmail.Text,
                    MatKhau = txtMatKhau.Text,
                    GioiTinh = radNam.Checked ? true : false,
                    Luong = Convert.ToDecimal(txtLuong.Text),
                    NgayBatDau = dtpNgayBatDau.Value,
                    LoaiNV = 0
                };
                if (nvBUS.themNhanVien(nvDTO))
                {
                    ThongBao.themthanhcong();
                }
                else ThongBao.nvdatontai();
                this.Close();
            }
            else ThongBao.nhapthieuduieu();
        }
    }
}
