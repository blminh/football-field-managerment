﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;
namespace QLSB
{
    public partial class frmSuaNhanVien : DevExpress.XtraEditors.XtraForm
    {
        public frmSuaNhanVien()
        {
            InitializeComponent();
            load();
        }
        NhanVienBUS nvBUS = new NhanVienBUS();
        public void load()
        {
            gridView1.OptionsBehavior.AutoPopulateColumns = false;
            gvSuaNhanVien.DataSource = nvBUS.loadDSNhanVien();
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            load();
            txtEmail.ResetText();
            txtHo.ResetText();
            txtMatKhau.ResetText();
            txtTen.ResetText();
            txtSDT.ResetText();
            cboGioiTinh.ResetText();
            txtLuong.ResetText();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            NhanVienDTO nvDTO = new NhanVienDTO
            {
                MaNV = txtMa.Text,
                Ho = txtHo.Text,
                Ten = txtTen.Text,
                MatKhau = txtMatKhau.Text,
                Email = txtEmail.Text,
                SoDienThoai = txtSDT.Text,
                GioiTinh = cboGioiTinh.EditValue.ToString() == "Nam" ? true : false,
                Luong = Convert.ToDecimal(txtLuong.Text)
            };
            if (nvBUS.suaNhanVien(nvDTO))
                ThongBao.suathanhcong();
            else ThongBao.suathatbai();
            load();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            nvBUS.xoaNhanVien(txtMa.Text);
            if (txtLoaiNV.Text == "Nhân viên")
                ThongBao.xoathanhcong();
            else if (txtLoaiNV.Text == "Quản lý")
                ThongBao.khongthexoaquyenhan();
            load();
        }
        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            txtMa.Text = gridView1.GetFocusedRowCellValue(grcMa).ToString();
            NhanVienDTO nv = new NhanVienDTO();
            nv = nvBUS.timNhanVien(txtMa.Text);
            txtHo.Text = nv.Ho;
            txtTen.Text = nv.Ten;
            txtEmail.Text = nv.Email;
            txtSDT.Text = nv.SoDienThoai;
            txtMatKhau.Text = nv.MatKhau;
            txtLuong.Text = Convert.ToString(nv.Luong);
            cboGioiTinh.EditValue = nv.GioiTinh == true ? "Nam" : "Nữ";
            txtLoaiNV.EditValue = nv.LoaiNV == 1 ? "Quản lý" : "Nhân viên";
        }

    }
}
