﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
namespace QLSB
{
    public partial class frmDanhSachNhanVien : Form
    {
        public frmDanhSachNhanVien()
        {
            InitializeComponent();
            load();
        }
        NhanVienBUS nvBUS = new NhanVienBUS();
        public void load()
        {
            gridView1.OptionsBehavior.AutoPopulateColumns = false;
            gvDSNhanVien.DataSource = nvBUS.loadDSNhanVien();
        }
        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            load();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
