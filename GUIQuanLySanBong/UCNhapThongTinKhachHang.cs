﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSB
{
    public partial class UCNhapThongTinKhachHang : DevExpress.XtraEditors.XtraUserControl
    {
        public UCNhapThongTinKhachHang()
        {
            InitializeComponent();
        }

        [Category ("Data"), Description ("Ho ten khach hang")]
        public string HoTen
        {
            get { return this.txtHoTenKH.Text; }
            set { this.txtHoTenKH.Text = value; }
        }

        [Category("Data"), Description("Mã khách hàng")]
        public string MaKH { get; set; }

        [Category("Data"), Description("So dien thoai")]
        public string SoDienThoai
        {
            get { return this.txt_sdt.Text; }
            set { this.txt_sdt.Text = value; }
        }
    }
}
