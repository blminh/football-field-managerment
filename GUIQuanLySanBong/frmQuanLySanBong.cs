﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using BUS;
using DTO;
using DevExpress.CodeParser;
using System.Security.Cryptography;

namespace QLSB
{
    public partial class frmQuanLySanBong : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmQuanLySanBong()
        {
            InitializeComponent();

            //turnOnOff(0);
            this.IsMdiContainer = true;
            this.lblTaiKhoan.Hide();
        }
        NhanVienBUS nvBUS = new NhanVienBUS();
        NhanVienDTO nvDTO = new NhanVienDTO();
        public static string hoTenNV = null;
        public void turnOnOff(int signal)
        {
            if(signal == 0)
            {
                ribSystem.Visible = false;
                ribManager.Visible = false;
            }
            else if(signal == 1)
            {
                ribSystem.Visible = true;
                ribManager.Visible = true;
            }
            else if (signal == 2)
            {
                ribSystem.Visible = true;
                ribManager.Visible = false;
            }
        }
        private void QuanLySanBong_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr;
            dr = XtraMessageBox.Show(config.exit, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        int trangthai = 0;
        private void barbtnDangNhap_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false;
                frmDangNhap _frmDangNhap = new frmDangNhap();
                _frmDangNhap.MdiParent = this;
                _frmDangNhap.Show();
                _frmDangNhap.sendNameForm += _frmDangNhap_sendNameForm;
        }

        public string MaHoa(string pass)
        {

            string convertMD5 = "";
            byte[] bytes = Encoding.UTF8.GetBytes(pass);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            bytes = md5.ComputeHash(bytes);
            for (int i = 0; i < bytes.Length; i++)
            {
                convertMD5 = convertMD5 + bytes[i].ToString("x2");
            }
            return convertMD5;
        }

        private void _frmDangNhap_sendNameForm(string tk,string mk)
        {
            lblTaiKhoan.Visible = true;
            nvDTO = nvBUS.timNhanVien(tk);
            
            if (nvDTO.MatKhau == MaHoa(mk) && nvDTO.LoaiNV == 1)
            {
                trangthai = 1;
                ThongBao.dangnhapthanhcong();
                turnOnOff(1);
                this.barbtnThongtin.Enabled = true;
                lblTaiKhoanDangNhap.Text = nvDTO.Ho + " " + nvDTO.Ten + " đang sử dụng hệ thống.";
                barbtnDangNhap.Enabled = false;
                barbtnDangXuat.Enabled = true;
                hoTenNV = nvDTO.Ho + " " + nvDTO.Ten;
            }
            else if (nvDTO.MatKhau == MaHoa(mk) && nvDTO.LoaiNV == 0)
            {
                trangthai = 1;
                ThongBao.dangnhapthanhcong();
                turnOnOff(2);
                this.barbtnThongtin.Enabled = true;
                lblTaiKhoanDangNhap.Text = nvDTO.Ho + " " + nvDTO.Ten + " đang sử dụng hệ thống.";
                barbtnDangNhap.Enabled = false;
                barbtnDangXuat.Enabled = true;
                hoTenNV = nvDTO.Ho + " " + nvDTO.Ten;
            }
            else ThongBao.saithongtinmk();

            if(nvDTO.LoaiNV == 1)
            {
                barbtnQuanLyNhanSu.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K));
                barbtnThemNV.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.J));
                barbtnSuaNV.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L));
                barBtnDatSan.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.F1));
                barbtnLichDatSan.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.F3));
                barbtnThongTinKhachHang.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.F4));
                barbtnThemKH.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.F5));
            }
            else if(nvDTO.LoaiNV == 0)
            {
                barBtnDatSan.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.F1));
                barbtnLichDatSan.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.F3));
                barbtnThongTinKhachHang.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.F4));
                barbtnThemKH.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.F5));
            }
        }

        private void barbtnDangXuat_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult dr = XtraMessageBox.Show(config.exit, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dr == DialogResult.Yes)
            {
                turnOnOff(0);
                barbtnDangNhap.Enabled = true;
                barbtnDangXuat.Enabled = false;
                lblTaiKhoan.Visible=false;
                lblTaiKhoanDangNhap.Text = "";
                barbtnQuanLyNhanSu.ItemShortcut = null;
                barbtnThemNV.ItemShortcut = null;
                barbtnSuaNV.ItemShortcut = null;
                barBtnDatSan.ItemShortcut = null;
                barbtnLichDatSan.ItemShortcut = null;
                barbtnThongTinKhachHang.ItemShortcut = null;
                barbtnThemKH.ItemShortcut = null;
                foreach (Form item in this.MdiChildren)
                {
                    if (config.checkExistForm(item.Name, this.MdiChildren))
                    {
                        item.Close();
                    }
                }
            }
            else if(dr == DialogResult.No)
            {
                ribbon.Enabled = true;
            }
        }

        private void barBtnDatSan_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false;
            if (!config.checkExistForm("frmSanBong", this.MdiChildren))
            {
                frmSanBong _frmSanBong = new frmSanBong();
                _frmSanBong.MdiParent = this;
                _frmSanBong.WindowState = FormWindowState.Maximized;
                _frmSanBong.Show();
            }
            else config.activeChilForm("frmSanBong", this.MdiChildren);
        }

        private void barbtnThongTinKhachHang_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false;
            if (!config.checkExistForm("frmThongTinKhachHang", this.MdiChildren))
            {
                frmThongTinKhachHang _frmThongTinKhachHang = new frmThongTinKhachHang();
                _frmThongTinKhachHang.MdiParent = this;
                _frmThongTinKhachHang.WindowState = FormWindowState.Maximized;
                _frmThongTinKhachHang.StartPosition = FormStartPosition.CenterParent;
                _frmThongTinKhachHang.Show();
            }
            else config.activeChilForm("frmThongTinKhachHang", this.MdiChildren);
        }

        private void barbtnQuanLyNhanSu_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false;
            if (!config.checkExistForm("frmDanhSachNhanVien", this.MdiChildren))
            {
                frmDanhSachNhanVien _frmQuanLyNhanSu = new frmDanhSachNhanVien();
                _frmQuanLyNhanSu.MdiParent = this;
                _frmQuanLyNhanSu.WindowState = FormWindowState.Maximized;
                _frmQuanLyNhanSu.StartPosition = FormStartPosition.CenterParent;
                _frmQuanLyNhanSu.Show();
            }
            else config.activeChilForm("frmDanhSachNhanVien", this.MdiChildren);
        }
        private void barbtnSuaThongTinNhanVien_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false;
            if (!config.checkExistForm("frmSuaNhanVien", this.MdiChildren))
            {
                frmSuaNhanVien _frmSuaNhanVien = new frmSuaNhanVien();
                _frmSuaNhanVien.MdiParent = this;
                _frmSuaNhanVien.WindowState = FormWindowState.Maximized;
                _frmSuaNhanVien.StartPosition = FormStartPosition.CenterParent;
                _frmSuaNhanVien.Show();
            }
            else config.activeChilForm("frmSuaNhanVien", this.MdiChildren);

        }
        private void barbtnTaoTK_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false; 
            frmThemNhanVien _themNhanVien = new frmThemNhanVien();
            _themNhanVien.ShowDialog();
        }

        private void barbtnXemBangSan_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false;
            if (!config.checkExistForm("frmDSSanBong", this.MdiChildren))
            {
                frmDSSanBong _frmDSSanBong = new frmDSSanBong();
                _frmDSSanBong.MdiParent = this;
                _frmDSSanBong.Show();
            }
            else config.activeChilForm("frmDSSanBong", this.MdiChildren);
        }

        private void barbtnLichDatSan_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false;
            if (!config.checkExistForm("frmDSLichDatSan", this.MdiChildren))
            {
                frmDSLichDatSan _frmDSDatSan = new frmDSLichDatSan(nvDTO.MaNV);
                _frmDSDatSan.MdiParent = this;
                _frmDSDatSan.Show();
            }
            else config.activeChilForm("frmDSLichDatSan", this.MdiChildren);
        }

        private void barbtnThemKH_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmDangKyKhachThanhVien _frmTaoTaiKhoan = new frmDangKyKhachThanhVien();
            _frmTaoTaiKhoan.ShowDialog();
        }

        private void barbtnLichSuDatSan_ItemClick(object sender, ItemClickEventArgs e)
        {
            pnlShow.Visible = false;
            if (!config.checkExistForm("frmChonBaoCao", this.MdiChildren))
            {
                frmChonBaoCao _frmChonBaoCao = new frmChonBaoCao();
                _frmChonBaoCao.MdiParent = this;
                _frmChonBaoCao.Show();
            }
            else config.activeChilForm("frmChonBaoCao", this.MdiChildren);
        }
    }
}