﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO;
using DevExpress.CodeParser;
using DevExpress.XtraBars.Ribbon;
using System.Threading;

namespace QLSB
{
    public partial class frmDangNhap : DevExpress.XtraEditors.XtraForm
    {
        public delegate void returnForm(string tk, string mk);
        public returnForm sendNameForm;
        NhanVienBUS nvBUS = new NhanVienBUS();
        public frmDangNhap()
        {
            InitializeComponent();
         
        }
        public void load()
        {
        }
        void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (cboTaiKhoan.Text == "" && txtMatKhau.Text == "")
            {
                ThongBao.chuachontkvamk();
            }
            else if(cboTaiKhoan.Text == "" && txtMatKhau.Text != "")
            {
                ThongBao.chuachontk();
            }   
            else if(cboTaiKhoan.Text != "" && txtMatKhau.Text == "")
            {
                ThongBao.chuachonmk();
            }
            else
            {
                if (sendNameForm != null)
                {
                    sendNameForm(cboTaiKhoan.Text, txtMatKhau.Text.Trim());
                    this.progressBarControl1.Properties.Minimum = 0;
                    this.progressBarControl1.Properties.Maximum = 100;
                    this.progressBarControl1.Properties.Step = 1;
                    this.progressBarControl1.Properties.PercentView = true;
                    this.progressBarControl1.Properties.ShowTitle = true;

                    for (int i = 0; i < 100; i++)
                    {
                        Thread.Sleep(100);
                        this.progressBarControl1.PerformStep();
                        this.progressBarControl1.Update();
                    }
                    this.Close();
                }
            }
          
               

        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            foreach(string item in nvBUS.layDSTaiKhoan().Select(u => u.MaNV))
            {
                cboTaiKhoan.Properties.Items.Add(item);
            }
        }

       
    }
}