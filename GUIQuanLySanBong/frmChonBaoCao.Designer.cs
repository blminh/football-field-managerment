﻿namespace QLSB
{
    partial class frmChonBaoCao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radTatCaSP = new System.Windows.Forms.RadioButton();
            this.radTheoLoaiSP = new System.Windows.Forms.RadioButton();
            this.radNhomtheoLoaiSP = new System.Windows.Forms.RadioButton();
            this.radHoaDon = new System.Windows.Forms.RadioButton();
            this.btnXembaoCao = new DevExpress.XtraEditors.SimpleButton();
            this.cbbTheoLoai = new System.Windows.Forms.ComboBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.SuspendLayout();
            // 
            // radTatCaSP
            // 
            this.radTatCaSP.AutoSize = true;
            this.radTatCaSP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.radTatCaSP.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTatCaSP.Location = new System.Drawing.Point(455, 267);
            this.radTatCaSP.Name = "radTatCaSP";
            this.radTatCaSP.Size = new System.Drawing.Size(160, 27);
            this.radTatCaSP.TabIndex = 1;
            this.radTatCaSP.TabStop = true;
            this.radTatCaSP.Text = "Tất cả sân bóng";
            this.radTatCaSP.UseVisualStyleBackColor = false;
            // 
            // radTheoLoaiSP
            // 
            this.radTheoLoaiSP.AutoSize = true;
            this.radTheoLoaiSP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.radTheoLoaiSP.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTheoLoaiSP.Location = new System.Drawing.Point(455, 423);
            this.radTheoLoaiSP.Name = "radTheoLoaiSP";
            this.radTheoLoaiSP.Size = new System.Drawing.Size(184, 27);
            this.radTheoLoaiSP.TabIndex = 2;
            this.radTheoLoaiSP.TabStop = true;
            this.radTheoLoaiSP.Text = "Theo loại sân bóng";
            this.radTheoLoaiSP.UseVisualStyleBackColor = false;
            // 
            // radNhomtheoLoaiSP
            // 
            this.radNhomtheoLoaiSP.AutoSize = true;
            this.radNhomtheoLoaiSP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.radNhomtheoLoaiSP.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNhomtheoLoaiSP.Location = new System.Drawing.Point(695, 267);
            this.radNhomtheoLoaiSP.Name = "radNhomtheoLoaiSP";
            this.radNhomtheoLoaiSP.Size = new System.Drawing.Size(232, 27);
            this.radNhomtheoLoaiSP.TabIndex = 4;
            this.radNhomtheoLoaiSP.TabStop = true;
            this.radNhomtheoLoaiSP.Text = "Nhóm theo loại sân bóng";
            this.radNhomtheoLoaiSP.UseVisualStyleBackColor = false;
            // 
            // radHoaDon
            // 
            this.radHoaDon.AutoSize = true;
            this.radHoaDon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.radHoaDon.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radHoaDon.Location = new System.Drawing.Point(702, 423);
            this.radHoaDon.Name = "radHoaDon";
            this.radHoaDon.Size = new System.Drawing.Size(192, 27);
            this.radHoaDon.TabIndex = 5;
            this.radHoaDon.TabStop = true;
            this.radHoaDon.Text = "Doanh thu sân bóng";
            this.radHoaDon.UseVisualStyleBackColor = false;
            // 
            // btnXembaoCao
            // 
            this.btnXembaoCao.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXembaoCao.Appearance.Options.UseFont = true;
            this.btnXembaoCao.Location = new System.Drawing.Point(604, 323);
            this.btnXembaoCao.Name = "btnXembaoCao";
            this.btnXembaoCao.Size = new System.Drawing.Size(125, 62);
            this.btnXembaoCao.TabIndex = 6;
            this.btnXembaoCao.Text = "Xem báo cáo";
            this.btnXembaoCao.Click += new System.EventHandler(this.btnXembaoCao_Click);
            // 
            // cbbTheoLoai
            // 
            this.cbbTheoLoai.FormattingEnabled = true;
            this.cbbTheoLoai.Location = new System.Drawing.Point(455, 456);
            this.cbbTheoLoai.Name = "cbbTheoLoai";
            this.cbbTheoLoai.Size = new System.Drawing.Size(184, 21);
            this.cbbTheoLoai.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Times New Roman", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(471, 66);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(160, 37);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "BÁO CÁO ";
            // 
            // frmChonBaoCao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Center;
            this.BackgroundImageStore = global::QLSB.Properties.Resources.field1;
            this.ClientSize = new System.Drawing.Size(1102, 528);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.cbbTheoLoai);
            this.Controls.Add(this.btnXembaoCao);
            this.Controls.Add(this.radHoaDon);
            this.Controls.Add(this.radNhomtheoLoaiSP);
            this.Controls.Add(this.radTheoLoaiSP);
            this.Controls.Add(this.radTatCaSP);
            this.DoubleBuffered = true;
            this.Name = "frmChonBaoCao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Xem báo cáo ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmChonBaoCao_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radTatCaSP;
        private System.Windows.Forms.RadioButton radTheoLoaiSP;
        private System.Windows.Forms.RadioButton radNhomtheoLoaiSP;
        private System.Windows.Forms.RadioButton radHoaDon;
        private DevExpress.XtraEditors.SimpleButton btnXembaoCao;
        private System.Windows.Forms.ComboBox cbbTheoLoai;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}