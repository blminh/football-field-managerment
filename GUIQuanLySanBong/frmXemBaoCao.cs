﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;
using Microsoft.Reporting.WinForms;

namespace QLSB
{
    public partial class frmXemBaoCao : DevExpress.XtraEditors.XtraForm
    {
        public frmXemBaoCao()
        {
            InitializeComponent();
        }

        SanBongBUS sbBus = new SanBongBUS();
        BangGiaBUS bgBus = new BangGiaBUS();
        ChiTietDatSanBUS dsbus = new ChiTietDatSanBUS();

        public void TatCaSanBong()
        { 
            // Lấy danh sách sân bóng
            List<SanBongDTO> lstSanBong = sbBus.layDSSanBong();
            // CHọn report cho reportviewer
            this.rptAll.LocalReport.ReportEmbeddedResource = "QLSB.rptTatCaSanPham.rdlc";

            //Đổ dữ liệu
            this.rptAll.LocalReport.DataSources.Add(new ReportDataSource("SanBong", lstSanBong));

            this.rptAll.RefreshReport();
        }
        public void TheoLoai(BangGiaDTO loaisb)
        {
            // Lấy danh sách sân bóng
            List<SanBongDTO> lstSanBong = (sbBus.layDSSanBongTheoLoai(loaisb.LoaiSanBong));
            // CHọn report cho reportviewer
            this.rptAll.LocalReport.ReportEmbeddedResource = "QLSB.rptTheoLoai.rdlc";

            //Đổ dữ liệu
            this.rptAll.LocalReport.DataSources.Add(new ReportDataSource("LoaiSanBong", lstSanBong));
            this.rptAll.LocalReport.SetParameters(new ReportParameter("paTenLoai", Convert.ToString(loaisb.LoaiSanBong)));
            this.rptAll.RefreshReport();
        }
        public void NhomTheoLoai()
        {
            // Lấy danh sách sân bóng
            List<BangGiaDTO> lstloaiSanPham = bgBus.layDSBangGia();
            // CHọn report cho reportviewer
            this.rptAll.LocalReport.ReportEmbeddedResource = "QLSB.rptSanPhamGroup.rdlc";
            this.rptAll.LocalReport.SubreportProcessing +=
                new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
            //LocalReport_SubreportProcessing
            //Đổ dữ liệu
            this.rptAll.LocalReport.DataSources.Add(new ReportDataSource("DSLoaiSanPham", lstloaiSanPham));

            this.rptAll.RefreshReport();
        }

        private void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            byte maloai = byte.Parse(e.Parameters["paMaLoai"].Values[0]);
            e.DataSources.Add(new ReportDataSource("DSSanPham", sbBus.layDSSanBongTheoLoai(maloai)));
        }

        public void HoaDon()
        {
            // Lấy danh sách sân bóng
            List<ChiTietDatSanDTO> lstSanBong = dsbus.dsChiTiet();
            // CHọn report cho reportviewer
            this.rptAll.LocalReport.ReportEmbeddedResource = "QLSB.rptHoaDon.rdlc";

            //Đổ dữ liệu
            this.rptAll.LocalReport.DataSources.Add(new ReportDataSource("DSSanPham", lstSanBong));
            string hoTenNV = frmQuanLySanBong.hoTenNV;
            this.rptAll.LocalReport.SetParameters(new ReportParameter("paNguoiLap",hoTenNV));
            this.rptAll.LocalReport.SetParameters(new ReportParameter("paNgayLap",DateTime.Now.ToString()));
            this.rptAll.RefreshReport();
        }
    }
}
