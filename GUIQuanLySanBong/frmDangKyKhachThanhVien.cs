﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;

namespace QLSB
{
    public partial class frmDangKyKhachThanhVien : DevExpress.XtraEditors.XtraForm
    {
        public frmDangKyKhachThanhVien()
        {
            InitializeComponent();
        }
        KhachHangBUS khBUS = new KhachHangBUS();

        private void btnTaoTaiKhoan_Click(object sender, EventArgs e)
        {
            if (txtHo.Text != "" && txtTen.Text != "" && txtSDT.Text != "" && txtEmail.Text != "" && txtSDT.Text != "" && txtEmail.Text != "")
            {
                KhachHangDTO khDTO = new KhachHangDTO
                {
                    Ho = txtHo.Text,
                    Ten = txtTen.Text,
                    SoDienThoai = txtSDT.Text,
                    Email = txtEmail.Text,
                    GioiTinh = radNam.Checked ? true : false
                };
                if (khBUS.ThemKhachThanhVien(khDTO))
                {
                    txtHo.Text = "";
                    txtTen.Text = "";
                    txtSDT.Text = "";
                    txtEmail.Text = "";
                    ThongBao.themthanhcong();
                }

                else ThongBao.khachdatontai();
            }
            else ThongBao.nhapthieuduieu();
        }


    }
}
