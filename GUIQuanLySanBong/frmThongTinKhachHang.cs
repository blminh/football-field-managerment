﻿using BUS;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DTO;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace QLSB
{
    public partial class frmThongTinKhachHang : DevExpress.XtraEditors.XtraForm
    {
        public frmThongTinKhachHang()
        {
            InitializeComponent();
            load();
        }

        KhachHangBUS khBUS = new KhachHangBUS();
        public void load()
        {
            gridView1.OptionsBehavior.AutoPopulateColumns = false;
            gvDSKhachHang.DataSource = khBUS.loadDSKhachHang();
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (gridView1.GetFocusedRowCellValue(grcMa).ToString() == "KH00")
            {
                txtHoKH.Enabled = false;
                txtTenKH.Enabled = false;
                txtSoDienThoai.Enabled = false;
                txtE.Enabled = false;
            }
            else
            {
                txtHoKH.Enabled = true;
                txtTenKH.Enabled = true;
                txtSoDienThoai.Enabled = true;
                txtE.Enabled = true;
            }
            txtMaKh.Text = gridView1.GetFocusedRowCellValue(grcMa).ToString();
            KhachHangDTO kh = new KhachHangDTO();
            string loaikh = kh.LoaiKH.ToString();
            kh = khBUS.TimKiemThongTinKH(txtMaKh.Text);
            txtHoKH.Text = kh.Ho;
            txtTenKH.Text = kh.Ten;
            txtE.Text = kh.Email;
            txtSoDienThoai.Text = kh.SoDienThoai;

            switch (int.Parse(kh.LoaiKH.ToString()))
            {
                case 1:
                    txtLoaiKH.Text = "Thường";
                    break;
                case 2:
                    txtLoaiKH.Text = "Bạc";
                    break;
                case 3:
                    txtLoaiKH.Text = "Vàng";
                    break;
                case 4:
                    txtLoaiKH.Text = "Bạch kim";
                    break;
                case 5:
                    txtLoaiKH.Text = "Kim cương";
                    break;
                default:
                    txtLoaiKH.Text = "Vãng lai";
                    break;
            }

            cbbGioiTinh.EditValue = kh.GioiTinh == true ? "Nam" : "Nữ";
        }

        private void btnSua_Click_1(object sender, EventArgs e)
        {
            KhachHangDTO khDTO = new KhachHangDTO
            {
                MaKH = txtMaKh.Text,
                Ho = txtHoKH.Text,
                Ten = txtTenKH.Text,
                LoaiKH = khBUS.TimKiemThongTinKH(txtMaKh.Text).LoaiKH,
                Email = txtE.Text,
                SoDienThoai = txtSoDienThoai.Text,
                GioiTinh = cbbGioiTinh.EditValue.ToString() == "Nam" ? true : false,
                DIEMTICHLUY = khBUS.TimKiemThongTinKH(txtMaKh.Text).DIEMTICHLUY
            };
            if (khBUS.SuaThongTinKH(khDTO))
                ThongBao.suathanhcong();
            else ThongBao.suathatbai();
            load();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (khBUS.XoaThongTinKH(txtMaKh.Text))
                ThongBao.xoathanhcong();
            else ThongBao.xoathatbai();
            load();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            txtHoKH.ResetText();
            txtLoaiKH.ResetText();
            txtMaKh.ResetText();
            txtTenKH.ResetText();
            txtSoDienThoai.ResetText();
            txtE.ResetText();
            load();
        }
    }
}