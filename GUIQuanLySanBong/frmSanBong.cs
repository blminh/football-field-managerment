﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DevExpress.XtraBars.Navigation;
using DTO;

namespace QLSB
{
    public partial class frmSanBong : DevExpress.XtraEditors.XtraForm
    {
        public frmSanBong()
        {
            InitializeComponent();
        }

        SanBongBUS sbBUS = new SanBongBUS();

        private void frmSanBong_Load(object sender, EventArgs e)
        {
            loadSB();
        }

        void loadSB()
        {
            flpDSSanBong.Controls.Clear();
            dateFrom.DateTime = DateTime.Now;
            timeFrom.Time = DateTime.Now;
            btnDat.Enabled = false;
            ThoiGian tg = new ThoiGian
            {
                ThoiGianVao = new DateTime(dateFrom.DateTime.Year, dateFrom.DateTime.Month, dateFrom.DateTime.Day, timeFrom.Time.Hour, timeFrom.Time.Minute, timeFrom.Time.Second),
                ThoiGianRa = new DateTime(dateFrom.DateTime.Year, dateFrom.DateTime.Month, dateFrom.DateTime.Day, timeTo.Time.Hour, timeTo.Time.Minute, timeTo.Time.Second)
            };
            List<SanBongDTO> lstSanBong = sbBUS.layDSSanBong(tg);
            foreach (SanBongDTO sb in lstSanBong)
            {
                UCChiTietSanBong uc = new UCChiTietSanBong();
                uc.TenSanBong = sb.TenSanBong;
                uc.MaSan = sb.MaSanBong;
                uc.LoaiSan = sb.LoaiSanBong;
                uc.KhuVuc = sb.KhuVuc;
                flpDSSanBong.Controls.Add(uc);
            }
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            ThoiGian tg = new ThoiGian
            {
                ThoiGianVao = new DateTime(dateFrom.DateTime.Year, dateFrom.DateTime.Month, dateFrom.DateTime.Day, timeFrom.Time.Hour, timeFrom.Time.Minute, timeFrom.Time.Second),
                ThoiGianRa = new DateTime(dateFrom.DateTime.Year, dateFrom.DateTime.Month, dateFrom.DateTime.Day, timeTo.Time.Hour, timeTo.Time.Minute, timeTo.Time.Second)
            };
            List<SanBongDTO> lstSanBong = sbBUS.layDSSanBong(tg);
            flpDSSanBong.Controls.Clear();

            if (chkLoai5.Checked || chkLoai7.Checked || chkLoai11.Checked)
            {
                if (chkLoai5.Checked)
                {
                    foreach (SanBongDTO sb in lstSanBong)
                    {
                        UCChiTietSanBong uc = new UCChiTietSanBong();
                        uc.TenSanBong = sb.TenSanBong;
                        uc.MaSan = sb.MaSanBong;
                        uc.LoaiSan = sb.LoaiSanBong;
                        uc.KhuVuc = sb.KhuVuc;
                        if (uc.LoaiSan == 5)
                            flpDSSanBong.Controls.Add(uc);
                    }
                }
                if (chkLoai7.Checked)
                {
                    foreach (SanBongDTO sb in lstSanBong)
                    {
                        UCChiTietSanBong uc = new UCChiTietSanBong();
                        uc.TenSanBong = sb.TenSanBong;
                        uc.MaSan = sb.MaSanBong;
                        uc.LoaiSan = sb.LoaiSanBong;
                        uc.KhuVuc = sb.KhuVuc;
                        if (uc.LoaiSan == 7)
                            flpDSSanBong.Controls.Add(uc);
                    }
                }
                if (chkLoai11.Checked)
                {
                    foreach (SanBongDTO sb in lstSanBong)
                    {
                        UCChiTietSanBong uc = new UCChiTietSanBong();
                        uc.TenSanBong = sb.TenSanBong;
                        uc.MaSan = sb.MaSanBong;
                        uc.LoaiSan = sb.LoaiSanBong;
                        uc.KhuVuc = sb.KhuVuc;
                        if (uc.LoaiSan == 11)
                            flpDSSanBong.Controls.Add(uc);
                    }
                }
            }
            else
            {
                foreach (SanBongDTO sb in lstSanBong)
                {
                    UCChiTietSanBong uc = new UCChiTietSanBong();
                    uc.TenSanBong = sb.TenSanBong;
                    uc.MaSan = sb.MaSanBong;
                    uc.LoaiSan = sb.LoaiSanBong;
                    uc.KhuVuc = sb.KhuVuc;
                    flpDSSanBong.Controls.Add(uc);
                }
            }
        }

        private void dateFrom_EditValueChanged(object sender, EventArgs e)
        {
            if(dateFrom.DateTime < DateTime.Now.Add(new TimeSpan(-1, 0, 0, 0)))
            {
                ThongBao.saingay();
                dateFrom.DateTime = DateTime.Now;
            }
        }

        private void timeFrom_EditValueChanged(object sender, EventArgs e)
        {
            if(dateFrom.DateTime == DateTime.Now)
            {
                if (timeFrom.Time < DateTime.Now.Add(new TimeSpan(0, -10, 0)))
                {
                    ThongBao.saithoigian();
                    timeFrom.Time = DateTime.Now;
                    return;
                }
                if (timeFrom.Time.Hour >= 22 && timeFrom.Time.ToString("tt", System.Globalization.CultureInfo.InvariantCulture) == "PM")
                {
                    ThongBao.saithoigian();
                    dateFrom.DateTime = DateTime.Now.AddDays(1);
                    timeFrom.Time = new DateTime(dateFrom.DateTime.Year, dateFrom.DateTime.Month, dateFrom.DateTime.Day, 4, 0, 0);
                    timeTo.Time = timeFrom.Time.Add(new TimeSpan(1, 0, 0));
                    return;
                }
            }
            timeTo.Time = timeFrom.Time.Add(new TimeSpan(1, 0, 0));
            btnDat.Enabled = true;
        }

        private void timeTo_EditValueChanged(object sender, EventArgs e)
        {
            if(timeFrom.Time.Hour + 1 > timeTo.Time.Hour)
            {
                ThongBao.saigio();
                timeTo.Time = timeFrom.Time.Add(new TimeSpan(1, 0, 0));
            }
        }

        private void btnDat_Click(object sender, EventArgs e)
        {
            DateTime dtVao = new DateTime(dateFrom.DateTime.Year, dateFrom.DateTime.Month, dateFrom.DateTime.Day, timeFrom.Time.Hour, timeFrom.Time.Millisecond, timeFrom.Time.Second);
            DateTime dtRa = new DateTime(dateFrom.DateTime.Year, dateFrom.DateTime.Month, dateFrom.DateTime.Day, timeTo.Time.Hour, timeTo.Time.Millisecond, timeTo.Time.Second);
            int ThoiGianThue = dtRa.Hour * 60 + dtRa.Minute - (dtVao.Hour * 60 + dtVao.Minute);
            List<SanBongDTO> lstSanBongChon = new List<SanBongDTO>();
            List<SanBongDTO> lstSanBongDaLoc = sbBUS.layDSSanBong(new ThoiGian{ ThoiGianVao = dtVao, ThoiGianRa = dtRa });
            List<ThoiGian> lstTime = new List<ThoiGian>();
            try
            {
                foreach (Control item in flpDSSanBong.Controls)
                {
                    UCChiTietSanBong uc = (UCChiTietSanBong)item;
                    SanBongDTO sb = new SanBongDTO
                    {
                        MaSanBong = uc.MaSan,
                        LoaiSanBong = uc.LoaiSan,
                        KhuVuc = uc.KhuVuc,
                        TenSanBong = uc.TenSanBong
                    };
                    ThoiGian tg = new ThoiGian
                    {
                        MaSan = uc.MaSan,
                        ThoiGianVao = dtVao,
                        ThoiGianRa = dtRa,
                        ThoiGianThue = ThoiGianThue
                    };
                    if (uc.ChonSanBong)
                    {
                        lstSanBongChon.Add(sb);
                        lstTime.Add(tg);
                    }
                }
                
                frmPhieuDatSan _frmPhieuDatSan = new frmPhieuDatSan();
                _frmPhieuDatSan.NhanDanhSach(lstSanBongChon, lstSanBongDaLoc, lstTime);
                _frmPhieuDatSan.ShowDialog();
            }
            catch (Exception)
            {
                ThongBao.saithoigian();
            }
            loadSB();
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            loadSB();
        }
    }
}
