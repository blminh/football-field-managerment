﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;

namespace QLSB
{
    public partial class frmThemSanBong : DevExpress.XtraEditors.XtraForm
    {
        public frmThemSanBong()
        {
            InitializeComponent();
        }

        BangGiaBUS bgBUS = new BangGiaBUS();
        private void frmThemSanBong_Load(object sender, EventArgs e)
        {
            foreach (int item in bgBUS.layDSBangGia().Select(u => u.LoaiSanBong))
            {
                cboLoaiSan.Properties.Items.Add(item);
            }

            foreach (string item in sbBUS.dskhuvuc())
            {
                cboKhuVuc.Properties.Items.Add(item);
            }
        }

        SanBongBUS sbBUS = new SanBongBUS();
        private void btnThem_Click(object sender, EventArgs e)
        {
            SanBongDTO sbDTO = new SanBongDTO();

            sbDTO.LoaiSanBong = Convert.ToByte(cboLoaiSan.EditValue.ToString());
            sbDTO.KhuVuc = cboKhuVuc.SelectedItem.ToString();

            if (sbBUS.ThemSanBong(sbDTO))
                ThongBao.thanhcong();
            else ThongBao.thatbai();
        }
    }
}
