﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DevExpress.XtraEditors;
using DTO;

namespace QLSB
{
    public partial class frmDSLichDatSan : DevExpress.XtraEditors.XtraForm
    {
        public frmDSLichDatSan()
        {
            InitializeComponent();
            loadDSDatSan();
        }

        public frmDSLichDatSan(string manv)
        {
            InitializeComponent();
            nhanvienSuDung = manv;
            loadDSDatSan();
        }

        string nhanvienSuDung = "";
        PhieuDatSanBUS pdsBUS = new PhieuDatSanBUS();
        KhachHangBUS khBUS = new KhachHangBUS();

        void loadDSDatSan()
        {
            gridView1.OptionsBehavior.AutoPopulateColumns = false;
            gvDSDatSan.DataSource = pdsBUS.dsDatSan();
        }

        PhieuDatSanDTO pdsLayRow = new PhieuDatSanDTO();
        
        
        private void btnThanhToan_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmHoaDon _frmHoaDon = new frmHoaDon();
            string sdt = gridView1.GetFocusedRowCellValue(colSoDienThoai).ToString();
            string tenkh = gridView1.GetFocusedRowCellValue(colKhachHang).ToString();
            string masan = gridView1.GetFocusedRowCellValue(colSan).ToString();
            DateTime dtVao = Convert.ToDateTime(gridView1.GetFocusedRowCellValue(colTimeF).ToString());
            DateTime dtRa = Convert.ToDateTime(gridView1.GetFocusedRowCellValue(colTimeT).ToString());
            int thoigianthue = dtRa.Hour*60+dtRa.Minute - dtVao.Hour*60+dtVao.Minute;
            string gia = gridView1.GetFocusedRowCellValue(colGiaTien).ToString();

            string makh = "";
            if (khBUS.KiemTraKhachThanhVien(sdt))
                makh = khBUS.LayKhachHangTheoSDT(sdt).MaKH;
            else makh = khBUS.loadDSKhachHang()[0].MaKH;

            PhieuDatSanDTO pdsDTO = new PhieuDatSanDTO
            {
                MaSan = masan,
                SoDienThoai = sdt,
                TenKH = tenkh,
                GioBatDau = dtVao,
                GioKetThuc = dtRa,
                ThoiGianThue = thoigianthue,
                GiaTien = Convert.ToDecimal(gia)
            };
            pdsLayRow = pdsDTO;

            _frmHoaDon.receiver(
                nhanvienSuDung,
                makh,
                pdsDTO
            );

            _frmHoaDon.ShowDialog();
            if (_frmHoaDon.XacNhan)
            {
                pdsBUS.ThanhToanPhieu(pdsLayRow);
                gridView1.DeleteRow(gridView1.FocusedRowHandle);
            }   
        }

        private void btnHuy_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DialogResult dig = MessageBox.Show("Bạn có chắc chắn hủy?", "Hủy sân", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dig == DialogResult.OK)
            {
                string sdt = gridView1.GetFocusedRowCellValue(colSoDienThoai).ToString();
                string masan = gridView1.GetFocusedRowCellValue(colSan).ToString();
                DateTime dtVao = Convert.ToDateTime(gridView1.GetFocusedRowCellValue(colTimeF).ToString());
                DateTime dtRa = Convert.ToDateTime(gridView1.GetFocusedRowCellValue(colTimeT).ToString());
                int thoigianthue = dtRa.Hour*60+dtRa.Minute - dtVao.Hour*60+dtVao.Minute;
                string gia = gridView1.GetFocusedRowCellValue(colGiaTien).ToString();

                string makh = "";
                if (khBUS.KiemTraKhachThanhVien(sdt))
                    makh = khBUS.LayKhachHangTheoSDT(sdt).MaKH;
                else makh = khBUS.loadDSKhachHang()[0].MaKH;

                PhieuDatSanDTO pdsDTO = new PhieuDatSanDTO
                {
                    MaSan = masan,
                    SoDienThoai = sdt,
                    GioBatDau = dtVao,
                    GioKetThuc = dtRa,
                    ThoiGianThue = thoigianthue,
                    GiaTien = Convert.ToDecimal(gia)
                };
                pdsLayRow = pdsDTO;
                pdsBUS.HuyPhieu(pdsLayRow);
                gridView1.DeleteRow(gridView1.FocusedRowHandle);
            }
        }

        private void radDon_CheckedChanged(object sender, EventArgs e)
        {
            if (radDon.Checked)
            {
                loadDSDatSan();
                gvDSDatSan.Enabled = true;
                btnTimPhieu.Enabled = false;
            }
        }

        private void radNhieu_CheckedChanged(object sender, EventArgs e)
        {
            if (radNhieu.Checked)
            {
                gvDSDatSan.Enabled = false;
                btnTimPhieu.Enabled = true;
            }
        }

        private void btnTimPhieu_Click(object sender, EventArgs e)
        {
            frmTimPhieu _frmTimPhieu = new frmTimPhieu();
            _frmTimPhieu.send += _frmTimPhieu_send;
            _frmTimPhieu.ShowDialog();
        }

        private void _frmTimPhieu_send(List<PhieuDatSanDTO> lst, string sdt)
        {
            string makh = "";
            if (khBUS.KiemTraKhachThanhVien(sdt))
                makh = khBUS.LayKhachHangTheoSDT(sdt).MaKH;
            else makh = khBUS.loadDSKhachHang()[0].MaKH;
            frmHoaDon _frmHoaDon = new frmHoaDon();
            _frmHoaDon.receivers(nhanvienSuDung, makh, lst);
            _frmHoaDon.ShowDialog();
            if(_frmHoaDon.XacNhan)
                foreach (PhieuDatSanDTO item in lst)
                {
                    pdsBUS.ThanhToanPhieu(item);
                }
        }

        private void txtLamMoi_Click(object sender, EventArgs e)
        {
            loadDSDatSan();
        }
    }
}
