﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.PivotGrid.OLAP.Mdx;

namespace QLSB
{
    public partial class UCChiTietSanBong : DevExpress.XtraEditors.XtraUserControl
    {
        public UCChiTietSanBong()
        {
            InitializeComponent();
        }

        [Category("Data"), Description("Field name")]
        public string TenSanBong
        {
            get { return this.lblTen.Text; }
            set { this.lblTen.Text = value; }
        }

        [Category("Data"), Description("Select")]
        public bool ChonSanBong {
            get { return this.chkChon.Checked; }
            set { this.chkChon.Checked = value; }
        }

        [Category("Data"), Description("Ma San Bong")]
        public string MaSan { get; set; }

        [Category("Data"), Description("Loai San Bong")]
        public byte LoaiSan { get; set; }

        [Category("Data"), Description("Khu vuc san bong")]
        public string KhuVuc { get; set; }

        private void UCChiTietSanPham_Click(object sender, EventArgs e)
        {
            if (chkChon.Checked)
                chkChon.Checked = false;
            else chkChon.Checked = true;
        }
    }
}
