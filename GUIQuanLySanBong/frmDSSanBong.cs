﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;

namespace QLSB
{
    public partial class frmDSSanBong : DevExpress.XtraEditors.XtraForm
    {
        public frmDSSanBong()
        {
            InitializeComponent();
            loadSB();
        }
        SanBongBUS sbBUS = new SanBongBUS();
        public void loadSB()
        {
            gvDSSanBong.DataSource = sbBUS.layDSSanBong(1);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmThemSanBong _frmThemSanBong = new frmThemSanBong();
            _frmThemSanBong.ShowDialog();
            loadSB();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (sbBUS.XoaSanBong(gridView1.GetFocusedRowCellValue(colMa).ToString()))
                gridView1.DeleteRow(gridView1.FocusedRowHandle);
            else ThongBao.thaydoitrangthaisanthatbai();
        }

        private void btnBaoTri_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            SanBongDTO sb = sbBUS.laySanBongTheoMa(gridView1.GetFocusedRowCellValue(colMa).ToString());
            if (sbBUS.SuaSanBong(sb.MaSanBong))
            {
                if (sb.TrangThai == 1)
                    ThongBao.baotri();
                else ThongBao.baotrixong();
                gridView1.DeleteRow(gridView1.FocusedRowHandle);
            }
            else ThongBao.thaydoitrangthaisanthatbai();
        }

        private void btnSanBaoTri_Click(object sender, EventArgs e)
        {
            gvDSSanBong.DataSource = sbBUS.layDSSanBong(2);
        }

        private void btnDanhSach_Click(object sender, EventArgs e)
        {
            gvDSSanBong.DataSource = sbBUS.layDSSanBong(1);
        }
    }
}
