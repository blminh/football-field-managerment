﻿namespace QLSB
{
    partial class UCChiTietSanBong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTen = new DevExpress.XtraEditors.LabelControl();
            this.chkChon = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblTen
            // 
            this.lblTen.Appearance.BackColor = System.Drawing.Color.Black;
            this.lblTen.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTen.Appearance.ForeColor = System.Drawing.Color.FloralWhite;
            this.lblTen.Appearance.Options.UseBackColor = true;
            this.lblTen.Appearance.Options.UseFont = true;
            this.lblTen.Appearance.Options.UseForeColor = true;
            this.lblTen.Location = new System.Drawing.Point(82, 119);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(34, 19);
            this.lblTen.TabIndex = 0;
            this.lblTen.Text = "demo";
            // 
            // chkChon
            // 
            this.chkChon.AutoSize = true;
            this.chkChon.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkChon.Location = new System.Drawing.Point(183, 3);
            this.chkChon.Name = "chkChon";
            this.chkChon.Size = new System.Drawing.Size(15, 14);
            this.chkChon.TabIndex = 1;
            this.chkChon.UseVisualStyleBackColor = true;
            // 
            // UCChiTietSanBong
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::QLSB.Properties.Resources.trung;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.chkChon);
            this.Controls.Add(this.lblTen);
            this.DoubleBuffered = true;
            this.Name = "UCChiTietSanBong";
            this.Size = new System.Drawing.Size(201, 155);
            this.Click += new System.EventHandler(this.UCChiTietSanPham_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblTen;
        private System.Windows.Forms.CheckBox chkChon;
    }
}
