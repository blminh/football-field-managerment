﻿namespace QLSB
{
    partial class frmPhieuDatSan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPhieuDatSan));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.lblThongTin = new System.Windows.Forms.Label();
            this.btnDatSanHuy = new DevExpress.XtraEditors.SimpleButton();
            this.btnDatSan = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.cboLoai = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.ludSan = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gvDSSanDat = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMaSan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoaiSan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThoiGian = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.ucNhapThongTinKhachHang1 = new QLSB.UCNhapThongTinKhachHang();
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ludSan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDSSanDat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblThongTin
            // 
            this.lblThongTin.AutoSize = true;
            this.lblThongTin.Font = new System.Drawing.Font("Tahoma", 25F);
            this.lblThongTin.ForeColor = System.Drawing.Color.Red;
            this.lblThongTin.Location = new System.Drawing.Point(204, 9);
            this.lblThongTin.Name = "lblThongTin";
            this.lblThongTin.Size = new System.Drawing.Size(265, 41);
            this.lblThongTin.TabIndex = 34;
            this.lblThongTin.Text = "Phiếu Thông Tin";
            // 
            // btnDatSanHuy
            // 
            this.btnDatSanHuy.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDatSanHuy.Appearance.Options.UseFont = true;
            this.btnDatSanHuy.Location = new System.Drawing.Point(295, 164);
            this.btnDatSanHuy.Name = "btnDatSanHuy";
            this.btnDatSanHuy.Size = new System.Drawing.Size(81, 48);
            this.btnDatSanHuy.TabIndex = 5;
            this.btnDatSanHuy.Text = "Hủy";
            this.btnDatSanHuy.Click += new System.EventHandler(this.btnDatSanHuy_Click);
            // 
            // btnDatSan
            // 
            this.btnDatSan.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDatSan.Appearance.Options.UseFont = true;
            this.btnDatSan.Location = new System.Drawing.Point(178, 164);
            this.btnDatSan.Name = "btnDatSan";
            this.btnDatSan.Size = new System.Drawing.Size(82, 48);
            this.btnDatSan.TabIndex = 4;
            this.btnDatSan.Text = "Đặt sân";
            this.btnDatSan.Click += new System.EventHandler(this.btnDatSan_Click);
            // 
            // btnThem
            // 
            this.btnThem.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Appearance.Options.UseFont = true;
            this.btnThem.Location = new System.Drawing.Point(410, 164);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(81, 48);
            this.btnThem.TabIndex = 6;
            this.btnThem.Text = "Thêm sân";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // cboLoai
            // 
            this.cboLoai.EditValue = "";
            this.cboLoai.Location = new System.Drawing.Point(110, 36);
            this.cboLoai.Name = "cboLoai";
            this.cboLoai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboLoai.Size = new System.Drawing.Size(132, 20);
            this.cboLoai.TabIndex = 8;
            this.cboLoai.SelectedValueChanged += new System.EventHandler(this.cboLoai_SelectedValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(33, 37);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(58, 17);
            this.labelControl5.TabIndex = 53;
            this.labelControl5.Text = "Loại sân:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.ludSan);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.cboLoai);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Location = new System.Drawing.Point(410, 59);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(273, 99);
            this.groupControl1.TabIndex = 56;
            this.groupControl1.Text = "Chọn sân thêm";
            // 
            // ludSan
            // 
            this.ludSan.Location = new System.Drawing.Point(110, 72);
            this.ludSan.Name = "ludSan";
            this.ludSan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ludSan.Properties.NullText = "";
            this.ludSan.Size = new System.Drawing.Size(132, 20);
            this.ludSan.TabIndex = 57;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(33, 73);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 16);
            this.labelControl1.TabIndex = 54;
            this.labelControl1.Text = "Sân:";
            // 
            // gvDSSanDat
            // 
            this.gvDSSanDat.Location = new System.Drawing.Point(12, 218);
            this.gvDSSanDat.MainView = this.gridView1;
            this.gvDSSanDat.Name = "gvDSSanDat";
            this.gvDSSanDat.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit2});
            this.gvDSSanDat.Size = new System.Drawing.Size(672, 319);
            this.gvDSSanDat.TabIndex = 57;
            this.gvDSSanDat.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMaSan,
            this.colLoaiSan,
            this.colVao,
            this.colThoiGian,
            this.colXoa});
            this.gridView1.GridControl = this.gvDSSanDat;
            this.gridView1.Name = "gridView1";
            // 
            // colMaSan
            // 
            this.colMaSan.Caption = "Mã sân";
            this.colMaSan.FieldName = "d.MaSanBong";
            this.colMaSan.Name = "colMaSan";
            this.colMaSan.OptionsColumn.AllowEdit = false;
            this.colMaSan.OptionsColumn.ReadOnly = true;
            this.colMaSan.Visible = true;
            this.colMaSan.VisibleIndex = 0;
            // 
            // colLoaiSan
            // 
            this.colLoaiSan.Caption = "Loại sân";
            this.colLoaiSan.FieldName = "d.LoaiSanBong";
            this.colLoaiSan.Name = "colLoaiSan";
            this.colLoaiSan.OptionsColumn.AllowEdit = false;
            this.colLoaiSan.OptionsColumn.ReadOnly = true;
            this.colLoaiSan.Visible = true;
            this.colLoaiSan.VisibleIndex = 1;
            // 
            // colVao
            // 
            this.colVao.Caption = "Vào";
            this.colVao.FieldName = "ThoiGianVao";
            this.colVao.Name = "colVao";
            this.colVao.Visible = true;
            this.colVao.VisibleIndex = 2;
            // 
            // colThoiGian
            // 
            this.colThoiGian.Caption = "Thời gian thuê";
            this.colThoiGian.FieldName = "ThoiGianThue";
            this.colThoiGian.Name = "colThoiGian";
            this.colThoiGian.Visible = true;
            this.colThoiGian.VisibleIndex = 3;
            // 
            // colXoa
            // 
            this.colXoa.Caption = "Xóa";
            this.colXoa.ColumnEdit = this.repositoryItemButtonEdit2;
            this.colXoa.Name = "colXoa";
            this.colXoa.Visible = true;
            this.colXoa.VisibleIndex = 4;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit2_ButtonClick);
            // 
            // ucNhapThongTinKhachHang1
            // 
            this.ucNhapThongTinKhachHang1.HoTen = "";
            this.ucNhapThongTinKhachHang1.Location = new System.Drawing.Point(12, 59);
            this.ucNhapThongTinKhachHang1.MaKH = null;
            this.ucNhapThongTinKhachHang1.Name = "ucNhapThongTinKhachHang1";
            this.ucNhapThongTinKhachHang1.Size = new System.Drawing.Size(364, 99);
            this.ucNhapThongTinKhachHang1.SoDienThoai = "";
            this.ucNhapThongTinKhachHang1.TabIndex = 58;
            // 
            // frmPhieuDatSan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(696, 549);
            this.ControlBox = false;
            this.Controls.Add(this.ucNhapThongTinKhachHang1);
            this.Controls.Add(this.gvDSSanDat);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.btnDatSan);
            this.Controls.Add(this.btnDatSanHuy);
            this.Controls.Add(this.lblThongTin);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPhieuDatSan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phiếu đặt sân";
            this.Load += new System.EventHandler(this.frmDatSan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cboLoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ludSan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDSSanDat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblThongTin;
        private DevExpress.XtraEditors.SimpleButton btnDatSanHuy;
        private DevExpress.XtraEditors.SimpleButton btnDatSan;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.ComboBoxEdit cboLoai;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit ludSan;
        private DevExpress.XtraGrid.GridControl gvDSSanDat;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colMaSan;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaiSan;
        private DevExpress.XtraGrid.Columns.GridColumn colThoiGian;
        private DevExpress.XtraGrid.Columns.GridColumn colXoa;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private UCNhapThongTinKhachHang ucNhapThongTinKhachHang1;
        private DevExpress.XtraGrid.Columns.GridColumn colVao;
    }
}