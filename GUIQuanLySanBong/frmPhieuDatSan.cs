﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO;
using BUS;
using DevExpress.XtraGrid;

namespace QLSB
{
    public partial class frmPhieuDatSan : DevExpress.XtraEditors.XtraForm
    {
        public delegate void nhanMaSan(List<SanBongDTO> lstSanChon, List<SanBongDTO> lstSanBongDaLocTruoc, List<ThoiGian> lstTime);
        public nhanMaSan NhanDanhSach;

        public frmPhieuDatSan()
        {
            InitializeComponent();
            NhanDanhSach = new nhanMaSan(LayDuLieu);
        }

        SanBongBUS sbBUS = new SanBongBUS();
        BangGiaBUS bgBUS = new BangGiaBUS();
        PhieuDatSanBUS pdsBUS = new PhieuDatSanBUS();
        KhachHangBUS khBUS = new KhachHangBUS();

        List<SanBongDTO> lstSanChon = new List<SanBongDTO>();
        List<SanBongDTO> lstSanBongDaLocTruoc = new List<SanBongDTO>();
        List<SanBongDTO> lstSanBongChuaLoc = new List<SanBongDTO>();
        List<ThoiGian> lstTime = new List<ThoiGian>();

        int ThoiGianThue = 0;
        DateTime dtVao = new DateTime();
        DateTime dtRa = new DateTime();

        private void LayDuLieu(List<SanBongDTO> lstSanChon, List<SanBongDTO> lstSanBongDaLocTruoc, List<ThoiGian> lstTime)
        {
            dtVao = lstTime[0].ThoiGianVao;
            dtRa = lstTime[0].ThoiGianRa;
            ThoiGianThue = lstTime[0].ThoiGianThue;
            this.lstSanChon = lstSanChon;
            this.lstTime = lstTime;
            this.lstSanBongDaLocTruoc = lstSanBongDaLocTruoc;
        }
        
        private void frmDatSan_Load(object NhanDanhSach, EventArgs e)
        {
            loadDanhsachSanChon(lstSanChon, lstTime);
            this.lstSanBongChuaLoc = this.lstSanBongDaLocTruoc;
            loadDanhSachSanDaLoc(this.lstSanBongChuaLoc, lstSanChon);
            foreach (int item in bgBUS.layDSBangGia().Select(u => u.LoaiSanBong))
            {
                cboLoai.Properties.Items.Add(item);
            }
        }

        void loadDanhSachSanDaLoc(List<SanBongDTO> lstSanChuaLoc, List<SanBongDTO> lstSanBongChon)
        {
            if(lstSanBongChon!=null)
                foreach (SanBongDTO item in lstSanBongChon)
                {
                    lstSanChuaLoc.RemoveAt(lstSanChuaLoc.FindIndex(x => x.MaSanBong == item.MaSanBong));
                }
            
            ludSan.Properties.DataSource = lstSanChuaLoc;
            ludSan.Properties.DisplayMember = "TenSanBong";
        }

        void loadDanhsachSanChon(List<SanBongDTO> lstSanBongChon, List<ThoiGian> lstTimeChange)
        {
            gvDSSanDat.DataSource = "";
            var result = (from d in lstSanBongChon
                          join b in bgBUS.layDSBangGia() on d.LoaiSanBong equals b.LoaiSanBong
                          join t in lstTimeChange on d.MaSanBong equals t.MaSan
                          select new { d, b.Gia, t.ThoiGianVao, t.ThoiGianThue, t.ThoiGianRa }).ToList();
            gridView1.OptionsBehavior.AutoPopulateColumns = false;
            gvDSSanDat.BeginUpdate();
            gvDSSanDat.DataSource = result;
            gvDSSanDat.RefreshDataSource();
            gvDSSanDat.EndUpdate();
        }

        private void btnDatSanHuy_Click(object NhanDanhSach, EventArgs e)
        {
            this.Close();
        }

        private void cboLoai_SelectedValueChanged(object NhanDanhSach, EventArgs e)
        {
            ComboBoxEdit cbo = NhanDanhSach as ComboBoxEdit;
            if (cbo.SelectedIndex != -1)
            {
                ludSan.Properties.DataSource = "";
                List<SanBongDTO> lst = new List<SanBongDTO>();
                foreach (SanBongDTO item in lstSanBongDaLocTruoc)
                {
                    if (cbo.SelectedText == item.LoaiSanBong.ToString())
                    {
                        lst.Add(item);
                    }
                }
                ludSan.Properties.DataSource = lst;
            }
        }

        private void btnThem_Click(object NhanDanhSach, EventArgs e)
        {
            try
            {
                SanBongDTO sb = new SanBongDTO
                {
                    MaSanBong = ludSan.Properties.GetDataSourceValue("MaSanBong", ludSan.ItemIndex).ToString(),
                    LoaiSanBong = byte.Parse(ludSan.Properties.GetDataSourceValue("LoaiSanBong", ludSan.ItemIndex).ToString()),
                    TenSanBong = ludSan.Properties.GetDataSourceValue("TenSanBong", ludSan.ItemIndex).ToString(),
                    KhuVuc = ludSan.Properties.GetDataSourceValue("KhuVuc", ludSan.ItemIndex).ToString()
                };

                this.lstSanChon.Add(sb);
                List<SanBongDTO> lst = new List<SanBongDTO>();
                lst.Add(sb);
                this.lstTime.Add(new ThoiGian { MaSan = sb.MaSanBong, ThoiGianVao = dtVao, ThoiGianRa = dtRa, ThoiGianThue = this.ThoiGianThue });

                loadDanhsachSanChon(this.lstSanChon, this.lstTime);
                loadDanhSachSanDaLoc(this.lstSanBongChuaLoc, lst);
            }
            catch (Exception)
            {
                MessageBox.Show("Hết sân");
                throw;
            }
        }

        private void btnDatSan_Click(object NhanDanhSach, EventArgs e)
        {
            if (ucNhapThongTinKhachHang1.HoTen.Trim() == "" || ucNhapThongTinKhachHang1.SoDienThoai.Trim() == "")
            {
                ThongBao.nhapthieuduieu();
                return;
            }
                
            List<PhieuDatSanDTO> lstPhieu = new List<PhieuDatSanDTO>();
            var resl = (from d in lstSanChon
                        join b in bgBUS.layDSBangGia() on d.LoaiSanBong equals b.LoaiSanBong
                        join t in lstTime on d.MaSanBong equals t.MaSan
                        select new { d, b.Gia, t.ThoiGianVao, t.ThoiGianThue, t.ThoiGianRa }).ToList();
            foreach (var item in resl)
            {
                PhieuDatSanDTO phieu = new PhieuDatSanDTO
                {
                    MaSan = item.d.MaSanBong,
                    TenKH = ucNhapThongTinKhachHang1.HoTen,
                    SoDienThoai = ucNhapThongTinKhachHang1.SoDienThoai,
                    GiaTien = item.Gia,
                    GioBatDau = item.ThoiGianVao,
                    GioKetThuc = item.ThoiGianRa
                };
                lstPhieu.Add(phieu);
            }
            if (pdsBUS.ThemPhieuDatSan(lstPhieu))
            {
                ThongBao.datsanthanhcong();
                this.Close();
            }
            else ThongBao.datsanthatbai();
        }

        private void repositoryItemButtonEdit2_ButtonClick(object NhanDanhSach, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                string masan = gridView1.GetFocusedRowCellValue(colMaSan).ToString();

                this.lstSanBongDaLocTruoc.Add(this.lstSanChon[lstSanChon.FindIndex(x => x.MaSanBong == masan)]);
                this.lstSanChon.RemoveAt(lstSanChon.FindIndex(x => x.MaSanBong == masan));
                gridView1.DeleteRow(gridView1.FocusedRowHandle);
            }
            catch (Exception exce)
            {
                MessageBox.Show(exce.ToString());
            }
            
        }
    }
}