﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;

namespace QLSB
{
    public partial class frmChonBaoCao : DevExpress.XtraEditors.XtraForm
    {
        public frmChonBaoCao()
        {
            InitializeComponent();
        }

      

        private void btnXembaoCao_Click(object sender, EventArgs e)
        {
            if(radTatCaSP.Checked)
            {
                frmXemBaoCao frm = new frmXemBaoCao();
                frm.TatCaSanBong();
                frm.ShowDialog();
            }
            if (radTheoLoaiSP.Checked)
            {
                frmXemBaoCao frm = new frmXemBaoCao();
                frm.TheoLoai((BangGiaDTO)cbbTheoLoai.SelectedItem);
                frm.ShowDialog();
            }
            if (radNhomtheoLoaiSP.Checked)
            {
                frmXemBaoCao frm = new frmXemBaoCao();
                frm.NhomTheoLoai();
                frm.ShowDialog();
            }
            if (radHoaDon.Checked)
            {
                frmXemBaoCao frm = new frmXemBaoCao();
                frm.HoaDon();
                frm.ShowDialog();
            }
        }
        SanBongBUS bus = new SanBongBUS();
        BangGiaBUS bgbus = new BangGiaBUS();
        private void frmChonBaoCao_Load(object sender, EventArgs e)
        {
            cbbTheoLoai.DataSource = bgbus.layDSBangGia();
            cbbTheoLoai.DisplayMember = "LoaiSanBong";
            cbbTheoLoai.ValueMember = "LoaiSanBong";
            //  this.reportViewer1.RefreshReport();
        }
    }
}
