﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSB
{
    public partial class Dang_nhap : Form
    {
        public Dang_nhap()
        {
            InitializeComponent();
        }

        private void txt_tk_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        private void bt_dn_Click(object sender, EventArgs e)
        {
            string tk = txt_tk.Text;
            string mk = txt_mk.Text;
            if (tk == "admin" && mk == "admin")
            {
                this.Hide();
                QuanLySanBong main = new QuanLySanBong();
                main.Show();
            }
            else
                MessageBox.Show("Lỗi!!!");
        }

        private void bt_thoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
