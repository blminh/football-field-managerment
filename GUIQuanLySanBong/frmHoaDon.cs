﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;

namespace QLSB
{
    public partial class frmHoaDon : DevExpress.XtraEditors.XtraForm
    {
        public delegate void nhanHoaDons(string manv, string makh, List<PhieuDatSanDTO> lstPhieu);
        public nhanHoaDons receivers;
        public delegate void nhanHoaDon(string manv, string makh, PhieuDatSanDTO phieu);
        public nhanHoaDon receiver;

        public frmHoaDon()
        {
            InitializeComponent();
            receivers = new nhanHoaDons(LayDanhSachHoaDon);
            receiver = new nhanHoaDon(LayHoaDon);
        }

        string khachhang = "";
        string nvSuDung = "";
        List<PhieuDatSanDTO> nhanPhieus = new List<PhieuDatSanDTO>();
        HoaDonBUS hdBUS = new HoaDonBUS();
        ChiTietDatSanBUS ctdsBUS = new ChiTietDatSanBUS();
        PhieuDatSanBUS pdsBUS = new PhieuDatSanBUS();
        KhachHangBUS khBUS = new KhachHangBUS();
        NhanVienBUS nvBUS = new NhanVienBUS();

        private void LayHoaDon(string manv, string makh, PhieuDatSanDTO phieu)
        {
            nvSuDung = manv;
            khachhang = makh;
            nhanPhieus.Add(phieu);
        }

        private void LayDanhSachHoaDon(string manv, string makh, List<PhieuDatSanDTO> lstPhieu)
        {
            nvSuDung = manv;
            khachhang = makh;
            nhanPhieus = lstPhieu;
        }
        
        private void frmHoaDon_Load(object sender, EventArgs e)
        {
            dateNgayLap.DateTime = DateTime.Now;
            txtMaHD.Text = phatsinhmahd();
            txtMaNV.Text = nvSuDung;
            txtSDT.Text = nhanPhieus[0].SoDienThoai;
            gcDSSan.DataSource = nhanPhieus;
            switch (khBUS.KiemTraLoaiKhachHang(txtSDT.Text))
            {
                case 0: txtGiamGia.Text = "0";
                    break;
                case 1: txtGiamGia.Text = "5";
                    break;
                case 2:
                    txtGiamGia.Text = "10";
                    break;
                case 3:
                    txtGiamGia.Text = "15";
                    break;
                default: txtGiamGia.Text = "20";
                    break;
            }
                
            double tong = nhanPhieus.Sum(x=>Convert.ToDouble(x.GiaTien));
            tong = tong - tong * double.Parse(txtGiamGia.Text) / 100 + tong * double.Parse(txtThue.Text) / 100;
            txtTongTien.Text = tong.ToString();
        }

        string phatsinhmahd()
        {
            int sl = hdBUS.layDSHoaDon().Count+1;
            string ma = "HD0"+sl;
            return ma;
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            if(txtTienNhan.Text.Trim() == "")
            {
                MessageBox.Show("Chưa nhập số tiền nhận");
                return;
            }
            if(double.Parse(txtTienNhan.Text.Trim()) < double.Parse(txtTongTien.Text.Trim()))
            {
                MessageBox.Show("Nhập sai số tiền nhận");
                return;
            }
            HoaDonDTO hdDTO = new HoaDonDTO
            {
                MaHD = txtMaHD.Text,
                MaNV = txtMaNV.Text,
                MaKH = khachhang,
                NgayLap = dateNgayLap.DateTime
            };
            if (hdBUS.ThemHoaDon(hdDTO))
                ThongBao.thanhtoanthanhcong();
            else ThongBao.thanhtoanthatbai();
            foreach (PhieuDatSanDTO item in nhanPhieus)
            {
                ChiTietDatSanDTO ctdsDTO = new ChiTietDatSanDTO
                {
                    MaHD = txtMaHD.Text,
                    MaSanBong = item.MaSan,
                    GiaTien = item.GiaTien,
                    GioBatDau = item.GioBatDau,
                    GioKetThuc = item.GioKetThuc,
                    Hoten = item.TenKH,
                    SoDienThoai = item.SoDienThoai
                };
                ctdsBUS.ThemChiTietDatSan(ctdsDTO);
            }
            khBUS.ThangHangKhachHang(Convert.ToDecimal(txtTongTien.Text), khBUS.TimKiemThongTinKH(khachhang).SoDienThoai);

            exit = true;
            DialogResult dr = MessageBox.Show("Bạn có muốn in hóa đơn?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if(dr == DialogResult.OK)
            {
                printDocument1.Print();
                //printPreviewDialog1.Document = printDocument1;
                //printPreviewDialog1.ShowDialog();
            }
            
            this.Close();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString("Sân Bóng TMT", new Font("Arial", 20, FontStyle.Bold), Brushes.Black, new Point(350, 0));
            e.Graphics.DrawString("Ngày lập: " + dateNgayLap.DateTime.ToShortDateString(), new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, 30));
            e.Graphics.DrawString("Địa chỉ: 65 Huỳnh Thúc Kháng, P. Bến Nghé, Q.1, TP HCM", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, 50));
            e.Graphics.DrawString("Khách hàng: " + nhanPhieus[0].TenKH, new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, 70));
            e.Graphics.DrawString("Số điện thoại: " + nhanPhieus[0].SoDienThoai, new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, 90));
            e.Graphics.DrawString("-----------------------------------------------------------------------------------------------------------------------------------------------------", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, 110));
            e.Graphics.DrawString("Mã sân", new Font("Arial", 12, FontStyle.Bold), Brushes.Black, new Point(5, 130));
            e.Graphics.DrawString("Vào", new Font("Arial", 12, FontStyle.Bold), Brushes.Black, new Point(130, 130));
            e.Graphics.DrawString("Ra", new Font("Arial", 12, FontStyle.Bold), Brushes.Black, new Point(280, 130));
            e.Graphics.DrawString("Thời gian thuê", new Font("Arial", 12, FontStyle.Bold), Brushes.Black, new Point(400, 130));
            e.Graphics.DrawString("Giá tiền", new Font("Arial", 12, FontStyle.Bold), Brushes.Black, new Point(600, 130));
            int y = 140;
            foreach (PhieuDatSanDTO item in nhanPhieus)
            {
                y += 20;
                e.Graphics.DrawString(item.MaSan, new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, y));
                e.Graphics.DrawString(item.GioBatDau.ToShortDateString() + " " + item.GioBatDau.ToShortTimeString(), new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(100, y));
                e.Graphics.DrawString(item.GioKetThuc.ToShortDateString() + " " + item.GioKetThuc.ToShortTimeString(), new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(250, y));
                e.Graphics.DrawString(item.ThoiGianThue.ToString(), new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(440, y));
                e.Graphics.DrawString(item.GiaTien.ToString() + " VND", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(600, y));
            }
            e.Graphics.DrawString("----------------------------------------------------------------------------------------------------------------------------------------------------", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, y+20));
            e.Graphics.DrawString("Giảm giá : " + txtGiamGia.Text + " %", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(550, y + 40));
            e.Graphics.DrawString("Thuế : " + txtThue.Text + " %", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(550, y + 60));
            e.Graphics.DrawString("Tổng tiền: " + txtTongTien.Text + " VND", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(550, y + 80));
            e.Graphics.DrawString("Tiền đưa : " + txtTienNhan.Text + " VND", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(550, y + 100));
            e.Graphics.DrawString("Trả lại : " + txtTienTra.Text + " VND", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(550, y + 120));
            e.Graphics.DrawString("Nhân viên lập hóa đơn: " + nvBUS.timNhanVien(txtMaNV.Text).Ho + " " + nvBUS.timNhanVien(txtMaNV.Text).Ten, new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, y + 140));
            e.Graphics.DrawString("----------------------------------------------------------------------------------------------------------------------------------------------------", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, y + 160));
            e.Graphics.DrawString("Xin cảm ơn quý khách và hẹn gặp lại!", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(5, y + 180));
        }

        bool exit = false;
        public bool XacNhan
        {
            get { return exit; }
            set { exit = value; }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtTienNhan_EditValueChanged(object sender, EventArgs e)
        {
            txtTienTra.Text = (double.Parse(txtTienNhan.Text) - double.Parse(txtTongTien.Text)).ToString();
        }
    }
}
