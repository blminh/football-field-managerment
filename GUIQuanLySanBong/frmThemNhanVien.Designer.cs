﻿namespace QLSB
{
    partial class frmThemNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlThemNV = new DevExpress.XtraEditors.PanelControl();
            this.dtpNgayBatDau = new System.Windows.Forms.DateTimePicker();
            this.lblTen = new DevExpress.XtraEditors.LabelControl();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.txtSDT = new DevExpress.XtraEditors.TextEdit();
            this.radNu = new System.Windows.Forms.RadioButton();
            this.btnTaoTaiKhoan = new DevExpress.XtraEditors.SimpleButton();
            this.radNam = new System.Windows.Forms.RadioButton();
            this.lblSDT = new DevExpress.XtraEditors.LabelControl();
            this.txtMatKhau = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.lblNgayBatDau = new DevExpress.XtraEditors.LabelControl();
            this.lblHo = new DevExpress.XtraEditors.LabelControl();
            this.lblLuong = new DevExpress.XtraEditors.LabelControl();
            this.lblGioiTinh = new DevExpress.XtraEditors.LabelControl();
            this.lblMatKhau = new DevExpress.XtraEditors.LabelControl();
            this.txtHo = new DevExpress.XtraEditors.TextEdit();
            this.lblEmail = new DevExpress.XtraEditors.LabelControl();
            this.txtLuong = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlThemNV)).BeginInit();
            this.pnlThemNV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatKhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuong.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlThemNV
            // 
            this.pnlThemNV.Controls.Add(this.dtpNgayBatDau);
            this.pnlThemNV.Controls.Add(this.lblTen);
            this.pnlThemNV.Controls.Add(this.txtTen);
            this.pnlThemNV.Controls.Add(this.txtSDT);
            this.pnlThemNV.Controls.Add(this.radNu);
            this.pnlThemNV.Controls.Add(this.btnTaoTaiKhoan);
            this.pnlThemNV.Controls.Add(this.radNam);
            this.pnlThemNV.Controls.Add(this.lblSDT);
            this.pnlThemNV.Controls.Add(this.txtMatKhau);
            this.pnlThemNV.Controls.Add(this.txtEmail);
            this.pnlThemNV.Controls.Add(this.lblNgayBatDau);
            this.pnlThemNV.Controls.Add(this.lblHo);
            this.pnlThemNV.Controls.Add(this.lblLuong);
            this.pnlThemNV.Controls.Add(this.lblGioiTinh);
            this.pnlThemNV.Controls.Add(this.lblMatKhau);
            this.pnlThemNV.Controls.Add(this.txtHo);
            this.pnlThemNV.Controls.Add(this.lblEmail);
            this.pnlThemNV.Controls.Add(this.txtLuong);
            this.pnlThemNV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlThemNV.Location = new System.Drawing.Point(0, 0);
            this.pnlThemNV.Name = "pnlThemNV";
            this.pnlThemNV.Size = new System.Drawing.Size(318, 298);
            this.pnlThemNV.TabIndex = 18;
            // 
            // dtpNgayBatDau
            // 
            this.dtpNgayBatDau.Location = new System.Drawing.Point(123, 187);
            this.dtpNgayBatDau.Margin = new System.Windows.Forms.Padding(2);
            this.dtpNgayBatDau.Name = "dtpNgayBatDau";
            this.dtpNgayBatDau.Size = new System.Drawing.Size(171, 20);
            this.dtpNgayBatDau.TabIndex = 18;
            this.dtpNgayBatDau.Value = new System.DateTime(2020, 6, 28, 0, 0, 0, 0);
            // 
            // lblTen
            // 
            this.lblTen.Location = new System.Drawing.Point(22, 36);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(19, 13);
            this.lblTen.TabIndex = 17;
            this.lblTen.Text = "Tên";
            // 
            // txtTen
            // 
            this.txtTen.Location = new System.Drawing.Point(121, 33);
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(170, 20);
            this.txtTen.TabIndex = 1;
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(121, 59);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(170, 20);
            this.txtSDT.TabIndex = 2;
            // 
            // radNu
            // 
            this.radNu.AutoSize = true;
            this.radNu.Location = new System.Drawing.Point(198, 85);
            this.radNu.Name = "radNu";
            this.radNu.Size = new System.Drawing.Size(39, 17);
            this.radNu.TabIndex = 4;
            this.radNu.TabStop = true;
            this.radNu.Text = "Nữ";
            this.radNu.UseVisualStyleBackColor = true;
            // 
            // btnTaoTaiKhoan
            // 
            this.btnTaoTaiKhoan.Location = new System.Drawing.Point(108, 240);
            this.btnTaoTaiKhoan.Name = "btnTaoTaiKhoan";
            this.btnTaoTaiKhoan.Size = new System.Drawing.Size(114, 36);
            this.btnTaoTaiKhoan.TabIndex = 6;
            this.btnTaoTaiKhoan.Text = "Tạo tài khoản";
            this.btnTaoTaiKhoan.Click += new System.EventHandler(this.btnTaoTaiKhoan_Click);
            // 
            // radNam
            // 
            this.radNam.AutoSize = true;
            this.radNam.Location = new System.Drawing.Point(123, 85);
            this.radNam.Name = "radNam";
            this.radNam.Size = new System.Drawing.Size(47, 17);
            this.radNam.TabIndex = 3;
            this.radNam.TabStop = true;
            this.radNam.Text = "Nam";
            this.radNam.UseVisualStyleBackColor = true;
            // 
            // lblSDT
            // 
            this.lblSDT.Location = new System.Drawing.Point(22, 62);
            this.lblSDT.Name = "lblSDT";
            this.lblSDT.Size = new System.Drawing.Size(63, 13);
            this.lblSDT.TabIndex = 3;
            this.lblSDT.Text = "Số điện thoại";
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(123, 133);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.Size = new System.Drawing.Size(170, 20);
            this.txtMatKhau.TabIndex = 5;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(123, 108);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(170, 20);
            this.txtEmail.TabIndex = 5;
            // 
            // lblNgayBatDau
            // 
            this.lblNgayBatDau.Location = new System.Drawing.Point(24, 187);
            this.lblNgayBatDau.Name = "lblNgayBatDau";
            this.lblNgayBatDau.Size = new System.Drawing.Size(65, 13);
            this.lblNgayBatDau.TabIndex = 7;
            this.lblNgayBatDau.Text = "Ngày bắt đầu";
            // 
            // lblHo
            // 
            this.lblHo.Location = new System.Drawing.Point(22, 10);
            this.lblHo.Name = "lblHo";
            this.lblHo.Size = new System.Drawing.Size(14, 13);
            this.lblHo.TabIndex = 4;
            this.lblHo.Text = "Họ";
            // 
            // lblLuong
            // 
            this.lblLuong.Location = new System.Drawing.Point(24, 162);
            this.lblLuong.Name = "lblLuong";
            this.lblLuong.Size = new System.Drawing.Size(30, 13);
            this.lblLuong.TabIndex = 7;
            this.lblLuong.Text = "Lương";
            // 
            // lblGioiTinh
            // 
            this.lblGioiTinh.Location = new System.Drawing.Point(24, 85);
            this.lblGioiTinh.Name = "lblGioiTinh";
            this.lblGioiTinh.Size = new System.Drawing.Size(40, 13);
            this.lblGioiTinh.TabIndex = 5;
            this.lblGioiTinh.Text = "Giới tính";
            // 
            // lblMatKhau
            // 
            this.lblMatKhau.Location = new System.Drawing.Point(24, 136);
            this.lblMatKhau.Name = "lblMatKhau";
            this.lblMatKhau.Size = new System.Drawing.Size(45, 13);
            this.lblMatKhau.TabIndex = 7;
            this.lblMatKhau.Text = "Mật khẩu";
            // 
            // txtHo
            // 
            this.txtHo.Location = new System.Drawing.Point(121, 7);
            this.txtHo.Name = "txtHo";
            this.txtHo.Size = new System.Drawing.Size(170, 20);
            this.txtHo.TabIndex = 0;
            // 
            // lblEmail
            // 
            this.lblEmail.Location = new System.Drawing.Point(24, 111);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(25, 13);
            this.lblEmail.TabIndex = 7;
            this.lblEmail.Text = "Email";
            // 
            // txtLuong
            // 
            this.txtLuong.Location = new System.Drawing.Point(123, 158);
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtLuong.Properties.Items.AddRange(new object[] {
            "15000",
            "20000",
            "30000",
            "40000"});
            this.txtLuong.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtLuong.Size = new System.Drawing.Size(170, 20);
            this.txtLuong.TabIndex = 5;
            // 
            // frmThemNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 298);
            this.Controls.Add(this.pnlThemNV);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmThemNhanVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thêm nhân viên";
            ((System.ComponentModel.ISupportInitialize)(this.pnlThemNV)).EndInit();
            this.pnlThemNV.ResumeLayout(false);
            this.pnlThemNV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatKhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLuong.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlThemNV;
        private DevExpress.XtraEditors.LabelControl lblTen;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.TextEdit txtSDT;
        private System.Windows.Forms.RadioButton radNu;
        private DevExpress.XtraEditors.SimpleButton btnTaoTaiKhoan;
        private System.Windows.Forms.RadioButton radNam;
        private DevExpress.XtraEditors.LabelControl lblSDT;
        private DevExpress.XtraEditors.TextEdit txtMatKhau;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.LabelControl lblHo;
        private DevExpress.XtraEditors.LabelControl lblGioiTinh;
        private DevExpress.XtraEditors.LabelControl lblMatKhau;
        private DevExpress.XtraEditors.TextEdit txtHo;
        private DevExpress.XtraEditors.LabelControl lblEmail;
        private System.Windows.Forms.DateTimePicker dtpNgayBatDau;
        private DevExpress.XtraEditors.LabelControl lblNgayBatDau;
        private DevExpress.XtraEditors.LabelControl lblLuong;
        private DevExpress.XtraEditors.ComboBoxEdit txtLuong;
    }
}