﻿namespace QLSB
{
    partial class UCNhapThongTinKhachHang
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_thongtinkhachhang = new DevExpress.XtraEditors.GroupControl();
            this.txt_sdt = new DevExpress.XtraEditors.TextEdit();
            this.txtHoTenKH = new DevExpress.XtraEditors.TextEdit();
            this.lb_sdt = new DevExpress.XtraEditors.LabelControl();
            this.lb_hotenKH = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.group_thongtinkhachhang)).BeginInit();
            this.group_thongtinkhachhang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sdt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTenKH.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // group_thongtinkhachhang
            // 
            this.group_thongtinkhachhang.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.group_thongtinkhachhang.Appearance.Options.UseBackColor = true;
            this.group_thongtinkhachhang.AppearanceCaption.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group_thongtinkhachhang.AppearanceCaption.Options.UseFont = true;
            this.group_thongtinkhachhang.Controls.Add(this.txt_sdt);
            this.group_thongtinkhachhang.Controls.Add(this.txtHoTenKH);
            this.group_thongtinkhachhang.Controls.Add(this.lb_sdt);
            this.group_thongtinkhachhang.Controls.Add(this.lb_hotenKH);
            this.group_thongtinkhachhang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group_thongtinkhachhang.Location = new System.Drawing.Point(0, 0);
            this.group_thongtinkhachhang.Name = "group_thongtinkhachhang";
            this.group_thongtinkhachhang.Size = new System.Drawing.Size(359, 99);
            this.group_thongtinkhachhang.TabIndex = 41;
            this.group_thongtinkhachhang.Text = "Thông tin khách hàng";
            // 
            // txt_sdt
            // 
            this.txt_sdt.Location = new System.Drawing.Point(173, 72);
            this.txt_sdt.Name = "txt_sdt";
            this.txt_sdt.Size = new System.Drawing.Size(151, 20);
            this.txt_sdt.TabIndex = 5;
            // 
            // txtHoTenKH
            // 
            this.txtHoTenKH.Location = new System.Drawing.Point(173, 34);
            this.txtHoTenKH.Name = "txtHoTenKH";
            this.txtHoTenKH.Size = new System.Drawing.Size(151, 20);
            this.txtHoTenKH.TabIndex = 3;
            // 
            // lb_sdt
            // 
            this.lb_sdt.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_sdt.Appearance.Options.UseFont = true;
            this.lb_sdt.Location = new System.Drawing.Point(20, 71);
            this.lb_sdt.Name = "lb_sdt";
            this.lb_sdt.Size = new System.Drawing.Size(79, 19);
            this.lb_sdt.TabIndex = 2;
            this.lb_sdt.Text = "Số điện thoại";
            // 
            // lb_hotenKH
            // 
            this.lb_hotenKH.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_hotenKH.Appearance.Options.UseFont = true;
            this.lb_hotenKH.Location = new System.Drawing.Point(20, 36);
            this.lb_hotenKH.Name = "lb_hotenKH";
            this.lb_hotenKH.Size = new System.Drawing.Size(113, 19);
            this.lb_hotenKH.TabIndex = 0;
            this.lb_hotenKH.Text = "Họ tên khách hàng";
            // 
            // UCNhapThongTinKhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.group_thongtinkhachhang);
            this.Name = "UCNhapThongTinKhachHang";
            this.Size = new System.Drawing.Size(359, 99);
            ((System.ComponentModel.ISupportInitialize)(this.group_thongtinkhachhang)).EndInit();
            this.group_thongtinkhachhang.ResumeLayout(false);
            this.group_thongtinkhachhang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sdt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoTenKH.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl group_thongtinkhachhang;
        private DevExpress.XtraEditors.TextEdit txt_sdt;
        private DevExpress.XtraEditors.TextEdit txtHoTenKH;
        private DevExpress.XtraEditors.LabelControl lb_sdt;
        private DevExpress.XtraEditors.LabelControl lb_hotenKH;
    }
}
