﻿namespace QLSB
{
    partial class frmQuanLySanBong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQuanLySanBong));
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup2 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barbtnDangXuat = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnThongtin = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnDatSan = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.barbtnThongTinKhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnQuanLyNhanSu = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnThemNV = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnSuaNV = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnXemBangSan = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnLichSuDatSan = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnXemHoaDon = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnTongDoanhThu = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.skinPaletteRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.skinRibbonGalleryBarItem2 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.skinPaletteRibbonGalleryBarItem2 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.skinRibbonGalleryBarItem3 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.barbtnDangNhap = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnLichDatSan = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnThemKH = new DevExpress.XtraBars.BarButtonItem();
            this.barTaiKhoan = new DevExpress.XtraBars.BarHeaderItem();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemFontEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.barbtnLichSuDangNhap = new DevExpress.XtraBars.BarButtonItem();
            this.skinDropDownButtonItem1 = new DevExpress.XtraBars.SkinDropDownButtonItem();
            this.skinRibbonGalleryBarItem4 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.skinPaletteRibbonGalleryBarItem3 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.skinPaletteDropDownButtonItem1 = new DevExpress.XtraBars.SkinPaletteDropDownButtonItem();
            this.skinPaletteRibbonGalleryBarItem4 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.skinPaletteDropDownButtonItem2 = new DevExpress.XtraBars.SkinPaletteDropDownButtonItem();
            this.skinPaletteRibbonGalleryBarItem5 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.skinPaletteRibbonGalleryBarItem6 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.skinPaletteRibbonGalleryBarItem7 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.skinRibbonGalleryBarItem5 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.ribAccount = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribSystem = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribManager = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pnlShow = new DevExpress.XtraEditors.PanelControl();
            this.lblTaiKhoan = new DevExpress.XtraEditors.LabelControl();
            this.lblTaiKhoanDangNhap = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlShow)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 519);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(5);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1201, 23);
            // 
            // ribbon
            // 
            this.ribbon.BackColor = System.Drawing.Color.White;
            this.ribbon.ColorScheme = DevExpress.XtraBars.Ribbon.RibbonControlColorScheme.Blue;
            this.ribbon.Cursor = System.Windows.Forms.Cursors.Default;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.barbtnDangXuat,
            this.barbtnThongtin,
            this.barBtnDatSan,
            this.barEditItem1,
            this.barbtnThongTinKhachHang,
            this.barbtnQuanLyNhanSu,
            this.barbtnThemNV,
            this.barbtnSuaNV,
            this.barbtnXemBangSan,
            this.barbtnLichSuDatSan,
            this.barbtnXemHoaDon,
            this.barbtnTongDoanhThu,
            this.skinRibbonGalleryBarItem1,
            this.skinPaletteRibbonGalleryBarItem1,
            this.skinRibbonGalleryBarItem2,
            this.skinPaletteRibbonGalleryBarItem2,
            this.skinRibbonGalleryBarItem3,
            this.barbtnDangNhap,
            this.barbtnLichDatSan,
            this.barbtnThemKH,
            this.barTaiKhoan,
            this.barEditItem2,
            this.barbtnLichSuDangNhap,
            this.skinDropDownButtonItem1,
            this.skinRibbonGalleryBarItem4,
            this.skinPaletteRibbonGalleryBarItem3,
            this.skinPaletteDropDownButtonItem1,
            this.skinPaletteRibbonGalleryBarItem4,
            this.skinPaletteDropDownButtonItem2,
            this.skinPaletteRibbonGalleryBarItem5,
            this.skinPaletteRibbonGalleryBarItem6,
            this.ribbonGalleryBarItem1,
            this.skinPaletteRibbonGalleryBarItem7,
            this.skinRibbonGalleryBarItem5});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(5);
            this.ribbon.MaxItemId = 64;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribAccount,
            this.ribSystem,
            this.ribManager});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1,
            this.repositoryItemFontEdit1});
            this.ribbon.Size = new System.Drawing.Size(1201, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // barbtnDangXuat
            // 
            this.barbtnDangXuat.Caption = "Đăng xuất";
            this.barbtnDangXuat.Enabled = false;
            this.barbtnDangXuat.Id = 2;
            this.barbtnDangXuat.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barbtnDangXuat.ImageOptions.Image")));
            this.barbtnDangXuat.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barbtnDangXuat.ImageOptions.LargeImage")));
            this.barbtnDangXuat.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X));
            this.barbtnDangXuat.LargeWidth = 70;
            this.barbtnDangXuat.Name = "barbtnDangXuat";
            this.barbtnDangXuat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnDangXuat_ItemClick);
            // 
            // barbtnThongtin
            // 
            this.barbtnThongtin.Caption = "Thông tin";
            this.barbtnThongtin.Id = 3;
            this.barbtnThongtin.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barbtnThongtin.ImageOptions.Image")));
            this.barbtnThongtin.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barbtnThongtin.ImageOptions.LargeImage")));
            this.barbtnThongtin.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this.barbtnThongtin.LargeWidth = 70;
            this.barbtnThongtin.Name = "barbtnThongtin";
            // 
            // barBtnDatSan
            // 
            this.barBtnDatSan.Caption = "Đặt sân";
            this.barBtnDatSan.Id = 11;
            this.barBtnDatSan.ImageOptions.Image = global::QLSB.Properties.Resources.field;
            this.barBtnDatSan.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R));
            this.barBtnDatSan.LargeWidth = 70;
            this.barBtnDatSan.Name = "barBtnDatSan";
            this.barBtnDatSan.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barBtnDatSan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDatSan_ItemClick);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemImageEdit1;
            this.barEditItem1.Id = 16;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // barbtnThongTinKhachHang
            // 
            this.barbtnThongTinKhachHang.Caption = "Thông tin khách hàng";
            this.barbtnThongTinKhachHang.Id = 17;
            this.barbtnThongTinKhachHang.ImageOptions.Image = global::QLSB.Properties.Resources.contact_16x16;
            this.barbtnThongTinKhachHang.ImageOptions.LargeImage = global::QLSB.Properties.Resources.contact_32x32;
            this.barbtnThongTinKhachHang.LargeWidth = 70;
            this.barbtnThongTinKhachHang.Name = "barbtnThongTinKhachHang";
            this.barbtnThongTinKhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnThongTinKhachHang_ItemClick);
            // 
            // barbtnQuanLyNhanSu
            // 
            this.barbtnQuanLyNhanSu.Caption = "Quản lý nhân sự";
            this.barbtnQuanLyNhanSu.Id = 18;
            this.barbtnQuanLyNhanSu.ImageOptions.Image = global::QLSB.Properties.Resources.usergroup_16x16;
            this.barbtnQuanLyNhanSu.ImageOptions.LargeImage = global::QLSB.Properties.Resources.usergroup_32x32;
            this.barbtnQuanLyNhanSu.LargeWidth = 70;
            this.barbtnQuanLyNhanSu.Name = "barbtnQuanLyNhanSu";
            this.barbtnQuanLyNhanSu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnQuanLyNhanSu_ItemClick);
            // 
            // barbtnThemNV
            // 
            this.barbtnThemNV.Caption = "Thêm nhân viên";
            this.barbtnThemNV.Id = 19;
            this.barbtnThemNV.ImageOptions.Image = global::QLSB.Properties.Resources.edittask_16x16;
            this.barbtnThemNV.ImageOptions.LargeImage = global::QLSB.Properties.Resources.edittask_32x32;
            this.barbtnThemNV.LargeWidth = 70;
            this.barbtnThemNV.Name = "barbtnThemNV";
            this.barbtnThemNV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnTaoTK_ItemClick);
            // 
            // barbtnSuaNV
            // 
            this.barbtnSuaNV.Caption = "Sửa thông tin nhân viên";
            this.barbtnSuaNV.Id = 20;
            this.barbtnSuaNV.ImageOptions.Image = global::QLSB.Properties.Resources.apply_16x16;
            this.barbtnSuaNV.ImageOptions.LargeImage = global::QLSB.Properties.Resources.apply_32x32;
            this.barbtnSuaNV.LargeWidth = 75;
            this.barbtnSuaNV.Name = "barbtnSuaNV";
            this.barbtnSuaNV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnSuaThongTinNhanVien_ItemClick);
            // 
            // barbtnXemBangSan
            // 
            this.barbtnXemBangSan.Caption = "Xem bảng sân";
            this.barbtnXemBangSan.Id = 24;
            this.barbtnXemBangSan.ImageOptions.Image = global::QLSB.Properties.Resources.converttorange_16x16;
            this.barbtnXemBangSan.ImageOptions.LargeImage = global::QLSB.Properties.Resources.converttorange_32x32;
            this.barbtnXemBangSan.LargeWidth = 70;
            this.barbtnXemBangSan.Name = "barbtnXemBangSan";
            this.barbtnXemBangSan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnXemBangSan_ItemClick);
            // 
            // barbtnLichSuDatSan
            // 
            this.barbtnLichSuDatSan.Caption = "Báo Cáo";
            this.barbtnLichSuDatSan.Id = 29;
            this.barbtnLichSuDatSan.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barbtnLichSuDatSan.ImageOptions.Image")));
            this.barbtnLichSuDatSan.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barbtnLichSuDatSan.ImageOptions.LargeImage")));
            this.barbtnLichSuDatSan.LargeWidth = 70;
            this.barbtnLichSuDatSan.Name = "barbtnLichSuDatSan";
            this.barbtnLichSuDatSan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnLichSuDatSan_ItemClick);
            // 
            // barbtnXemHoaDon
            // 
            this.barbtnXemHoaDon.Caption = "Xem hóa đơn";
            this.barbtnXemHoaDon.Id = 31;
            this.barbtnXemHoaDon.ImageOptions.SvgImage = global::QLSB.Properties.Resources.alignfloatingobjectbottomleft;
            this.barbtnXemHoaDon.LargeWidth = 70;
            this.barbtnXemHoaDon.Name = "barbtnXemHoaDon";
            // 
            // barbtnTongDoanhThu
            // 
            this.barbtnTongDoanhThu.Caption = "Tổng doanh thu";
            this.barbtnTongDoanhThu.Id = 33;
            this.barbtnTongDoanhThu.ImageOptions.Image = global::QLSB.Properties.Resources._3dcolumn_16x16;
            this.barbtnTongDoanhThu.ImageOptions.LargeImage = global::QLSB.Properties.Resources._3dcolumn_32x32;
            this.barbtnTongDoanhThu.Name = "barbtnTongDoanhThu";
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 35;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // skinPaletteRibbonGalleryBarItem1
            // 
            this.skinPaletteRibbonGalleryBarItem1.Caption = "skinPaletteRibbonGalleryBarItem1";
            this.skinPaletteRibbonGalleryBarItem1.Id = 36;
            this.skinPaletteRibbonGalleryBarItem1.Name = "skinPaletteRibbonGalleryBarItem1";
            // 
            // skinRibbonGalleryBarItem2
            // 
            this.skinRibbonGalleryBarItem2.Caption = "skinRibbonGalleryBarItem2";
            this.skinRibbonGalleryBarItem2.Id = 37;
            this.skinRibbonGalleryBarItem2.Name = "skinRibbonGalleryBarItem2";
            // 
            // skinPaletteRibbonGalleryBarItem2
            // 
            this.skinPaletteRibbonGalleryBarItem2.Caption = "skinPaletteRibbonGalleryBarItem2";
            this.skinPaletteRibbonGalleryBarItem2.Id = 38;
            this.skinPaletteRibbonGalleryBarItem2.Name = "skinPaletteRibbonGalleryBarItem2";
            // 
            // skinRibbonGalleryBarItem3
            // 
            this.skinRibbonGalleryBarItem3.Caption = "skinRibbonGalleryBarItem3";
            this.skinRibbonGalleryBarItem3.Id = 39;
            this.skinRibbonGalleryBarItem3.Name = "skinRibbonGalleryBarItem3";
            // 
            // barbtnDangNhap
            // 
            this.barbtnDangNhap.Caption = "Đăng nhập";
            this.barbtnDangNhap.Id = 43;
            this.barbtnDangNhap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barbtnDangNhap.ImageOptions.Image")));
            this.barbtnDangNhap.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barbtnDangNhap.ImageOptions.LargeImage")));
            this.barbtnDangNhap.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D));
            this.barbtnDangNhap.Name = "barbtnDangNhap";
            this.barbtnDangNhap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnDangNhap_ItemClick);
            // 
            // barbtnLichDatSan
            // 
            this.barbtnLichDatSan.Caption = "Lịch đặt sân";
            this.barbtnLichDatSan.Id = 44;
            this.barbtnLichDatSan.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barbtnLichDatSan.ImageOptions.Image")));
            this.barbtnLichDatSan.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barbtnLichDatSan.ImageOptions.LargeImage")));
            this.barbtnLichDatSan.LargeWidth = 70;
            this.barbtnLichDatSan.Name = "barbtnLichDatSan";
            this.barbtnLichDatSan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnLichDatSan_ItemClick);
            // 
            // barbtnThemKH
            // 
            this.barbtnThemKH.Caption = "Đăng ký thành viên";
            this.barbtnThemKH.Id = 45;
            this.barbtnThemKH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barbtnThemKH.ImageOptions.Image")));
            this.barbtnThemKH.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barbtnThemKH.ImageOptions.LargeImage")));
            this.barbtnThemKH.LargeWidth = 70;
            this.barbtnThemKH.Name = "barbtnThemKH";
            this.barbtnThemKH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnThemKH_ItemClick);
            // 
            // barTaiKhoan
            // 
            this.barTaiKhoan.Caption = "Tài khoản :";
            this.barTaiKhoan.Id = 50;
            this.barTaiKhoan.Name = "barTaiKhoan";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemFontEdit1;
            this.barEditItem2.Id = 51;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemFontEdit1
            // 
            this.repositoryItemFontEdit1.AutoHeight = false;
            this.repositoryItemFontEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemFontEdit1.Name = "repositoryItemFontEdit1";
            // 
            // barbtnLichSuDangNhap
            // 
            this.barbtnLichSuDangNhap.Caption = "Lịch sử đăng nhập";
            this.barbtnLichSuDangNhap.Id = 52;
            this.barbtnLichSuDangNhap.Name = "barbtnLichSuDangNhap";
            // 
            // skinDropDownButtonItem1
            // 
            this.skinDropDownButtonItem1.Id = 53;
            this.skinDropDownButtonItem1.Name = "skinDropDownButtonItem1";
            // 
            // skinRibbonGalleryBarItem4
            // 
            this.skinRibbonGalleryBarItem4.Caption = "skinRibbonGalleryBarItem4";
            this.skinRibbonGalleryBarItem4.Id = 54;
            this.skinRibbonGalleryBarItem4.Name = "skinRibbonGalleryBarItem4";
            // 
            // skinPaletteRibbonGalleryBarItem3
            // 
            this.skinPaletteRibbonGalleryBarItem3.Caption = "skinPaletteRibbonGalleryBarItem3";
            // 
            // 
            // 
            galleryItemGroup1.Caption = "Group1";
            this.skinPaletteRibbonGalleryBarItem3.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.skinPaletteRibbonGalleryBarItem3.Id = 55;
            this.skinPaletteRibbonGalleryBarItem3.Name = "skinPaletteRibbonGalleryBarItem3";
            // 
            // skinPaletteDropDownButtonItem1
            // 
            this.skinPaletteDropDownButtonItem1.Enabled = false;
            this.skinPaletteDropDownButtonItem1.Id = 56;
            this.skinPaletteDropDownButtonItem1.Name = "skinPaletteDropDownButtonItem1";
            // 
            // skinPaletteRibbonGalleryBarItem4
            // 
            this.skinPaletteRibbonGalleryBarItem4.Caption = "skinPaletteRibbonGalleryBarItem4";
            this.skinPaletteRibbonGalleryBarItem4.Id = 57;
            this.skinPaletteRibbonGalleryBarItem4.Name = "skinPaletteRibbonGalleryBarItem4";
            // 
            // skinPaletteDropDownButtonItem2
            // 
            this.skinPaletteDropDownButtonItem2.Enabled = false;
            this.skinPaletteDropDownButtonItem2.Id = 58;
            this.skinPaletteDropDownButtonItem2.Name = "skinPaletteDropDownButtonItem2";
            // 
            // skinPaletteRibbonGalleryBarItem5
            // 
            this.skinPaletteRibbonGalleryBarItem5.Caption = "skinPaletteRibbonGalleryBarItem5";
            this.skinPaletteRibbonGalleryBarItem5.Id = 59;
            this.skinPaletteRibbonGalleryBarItem5.Name = "skinPaletteRibbonGalleryBarItem5";
            // 
            // skinPaletteRibbonGalleryBarItem6
            // 
            this.skinPaletteRibbonGalleryBarItem6.Caption = "skinPaletteRibbonGalleryBarItem6";
            // 
            // 
            // 
            galleryItemGroup2.Caption = "Group2";
            this.skinPaletteRibbonGalleryBarItem6.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup2});
            this.skinPaletteRibbonGalleryBarItem6.Id = 60;
            this.skinPaletteRibbonGalleryBarItem6.Name = "skinPaletteRibbonGalleryBarItem6";
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.Id = 61;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // skinPaletteRibbonGalleryBarItem7
            // 
            this.skinPaletteRibbonGalleryBarItem7.Caption = "skinPaletteRibbonGalleryBarItem7";
            this.skinPaletteRibbonGalleryBarItem7.Id = 62;
            this.skinPaletteRibbonGalleryBarItem7.Name = "skinPaletteRibbonGalleryBarItem7";
            // 
            // skinRibbonGalleryBarItem5
            // 
            this.skinRibbonGalleryBarItem5.Caption = "skinRibbonGalleryBarItem5";
            this.skinRibbonGalleryBarItem5.Id = 63;
            this.skinRibbonGalleryBarItem5.Name = "skinRibbonGalleryBarItem5";
            // 
            // ribAccount
            // 
            this.ribAccount.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribAccount.Name = "ribAccount";
            this.ribAccount.Text = "Tài khoản";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPageGroup1.ImageOptions.Image")));
            this.ribbonPageGroup1.ItemLinks.Add(this.barbtnDangNhap);
            this.ribbonPageGroup1.ItemLinks.Add(this.barbtnDangXuat);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Tài Khoản";
            // 
            // ribSystem
            // 
            this.ribSystem.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribSystem.Name = "ribSystem";
            this.ribSystem.Text = "Hệ Thống";
            this.ribSystem.Visible = false;
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPageGroup2.ImageOptions.Image")));
            this.ribbonPageGroup2.ItemLinks.Add(this.barBtnDatSan);
            this.ribbonPageGroup2.ItemLinks.Add(this.barbtnLichDatSan);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Chức năng";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.ItemLinks.Add(this.barbtnThongTinKhachHang);
            this.ribbonPageGroup3.ItemLinks.Add(this.barbtnThemKH);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Khách hàng";
            // 
            // ribManager
            // 
            this.ribManager.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6,
            this.ribbonPageGroup7});
            this.ribManager.Name = "ribManager";
            this.ribManager.Text = "Quản lý";
            this.ribManager.Visible = false;
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barbtnQuanLyNhanSu);
            this.ribbonPageGroup4.ItemLinks.Add(this.barbtnThemNV);
            this.ribbonPageGroup4.ItemLinks.Add(this.barbtnSuaNV);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "Quản lý tài khoản";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barbtnXemBangSan);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Quản lý chức năng";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barbtnLichSuDatSan);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            this.ribbonPageGroup6.Text = "Thống kê";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.skinRibbonGalleryBarItem5);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Giao diện";
            // 
            // pnlShow
            // 
            this.pnlShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlShow.Location = new System.Drawing.Point(0, 147);
            this.pnlShow.Name = "pnlShow";
            this.pnlShow.Size = new System.Drawing.Size(1201, 372);
            this.pnlShow.TabIndex = 2;
            // 
            // lblTaiKhoan
            // 
            this.lblTaiKhoan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTaiKhoan.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaiKhoan.Appearance.Options.UseFont = true;
            this.lblTaiKhoan.Location = new System.Drawing.Point(0, 518);
            this.lblTaiKhoan.Margin = new System.Windows.Forms.Padding(4);
            this.lblTaiKhoan.Name = "lblTaiKhoan";
            this.lblTaiKhoan.Size = new System.Drawing.Size(90, 18);
            this.lblTaiKhoan.TabIndex = 5;
            this.lblTaiKhoan.Text = "Người dùng:";
            this.lblTaiKhoan.Visible = false;
            // 
            // lblTaiKhoanDangNhap
            // 
            this.lblTaiKhoanDangNhap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTaiKhoanDangNhap.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaiKhoanDangNhap.Appearance.Options.UseFont = true;
            this.lblTaiKhoanDangNhap.Location = new System.Drawing.Point(116, 518);
            this.lblTaiKhoanDangNhap.Margin = new System.Windows.Forms.Padding(4);
            this.lblTaiKhoanDangNhap.Name = "lblTaiKhoanDangNhap";
            this.lblTaiKhoanDangNhap.Size = new System.Drawing.Size(0, 17);
            this.lblTaiKhoanDangNhap.TabIndex = 5;
            // 
            // frmQuanLySanBong
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.True;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1201, 542);
            this.Controls.Add(this.lblTaiKhoanDangNhap);
            this.Controls.Add(this.lblTaiKhoan);
            this.Controls.Add(this.pnlShow);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frmQuanLySanBong";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Quản Lý Sân Bóng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.QuanLySanBong_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlShow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barbtnDangXuat;
        private DevExpress.XtraBars.BarButtonItem barbtnThongtin;
        private DevExpress.XtraBars.BarButtonItem barBtnDatSan;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraBars.BarButtonItem barbtnThongTinKhachHang;
        private DevExpress.XtraBars.BarButtonItem barbtnQuanLyNhanSu;
        private DevExpress.XtraBars.BarButtonItem barbtnThemNV;
        private DevExpress.XtraBars.BarButtonItem barbtnSuaNV;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barbtnXemBangSan;
        private DevExpress.XtraBars.BarButtonItem barButtonItem26;
        private DevExpress.XtraBars.BarButtonItem barButtonItem27;
        private DevExpress.XtraBars.BarButtonItem barbtnLichSuDatSan;
        private DevExpress.XtraBars.BarButtonItem barbtnXemHoaDon;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem31;
        private DevExpress.XtraBars.BarButtonItem barbtnTongDoanhThu;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem2;
        private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem2;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem3;
        private DevExpress.XtraBars.BarButtonItem barbtnDangNhap;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribStart;
        private DevExpress.XtraEditors.PanelControl pnlShow;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barbtnLichDatSan;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribAccount;
        private DevExpress.XtraBars.BarButtonItem barbtnThemKH;
        public DevExpress.XtraBars.Ribbon.RibbonPage ribSystem;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribManager;
        private DevExpress.XtraBars.BarHeaderItem barTaiKhoan;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemFontEdit repositoryItemFontEdit1;
        private DevExpress.XtraEditors.LabelControl lblTaiKhoan;
        private DevExpress.XtraEditors.LabelControl lblTaiKhoanDangNhap;
        private DevExpress.XtraBars.BarButtonItem barbtnLichSuDangNhap;
        private DevExpress.XtraBars.SkinDropDownButtonItem skinDropDownButtonItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem4;
        private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem3;
        private DevExpress.XtraBars.SkinPaletteDropDownButtonItem skinPaletteDropDownButtonItem1;
        private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem4;
        private DevExpress.XtraBars.SkinPaletteDropDownButtonItem skinPaletteDropDownButtonItem2;
        private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem6;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem7;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem5;
    }
}