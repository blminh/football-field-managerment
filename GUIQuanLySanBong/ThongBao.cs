﻿using DevExpress.CodeParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSB
{
    public class ThongBao
    {
        //dkkhachthahvien
        public static void themthatbai()
        {
            MessageBox.Show("Thêm thất bại");
         
        }
        public static void themthanhcong()
        {
            MessageBox.Show("Thêm thành công");

        }
        public static void khachdatontai()
        {
            MessageBox.Show("Khách đã tồn tại");
        }
        public static void nhapthieuduieu()
        {
            MessageBox.Show("Nhập thiếu dữ liệu");
        }
        //thongtinkhachhang
        public static void suathanhcong()
        {
            MessageBox.Show("Bạn sửa thành công ");
        }
        public static void suathatbai()
        {
            MessageBox.Show("Bạn sửa thất bại  ");
        }
        public static void xoathatbai()
        {
            MessageBox.Show("Bạn xóa thất bại  "); 
        }
        public static void xoathanhcong()
        {
            MessageBox.Show("Bạn xóa thành công ");
        }
        //dangnhap
        public static void chuachontkvamk()
        {
            MessageBox.Show("Bạn chưa chọn tài khoản hoặc nhập mật khẩu");
        }
        public static void chuachontk()
        {
            MessageBox.Show("Bạn chưa chọn tài khoản");
        }
        public static void chuachonmk()
        {
            MessageBox.Show("Bạn chưa nhập mật khẩu");
        }
        public static int dangnhapthanhcong()
        {
            MessageBox.Show("Đăng nhập thành công");
            return 1;
        }
        public static int saithongtinmk()
        {
            MessageBox.Show("Sai thông tin mật khẩu");
            return 0;
        }
        //dsdatsan
        public static void thanhtoan()
        {
            MessageBox.Show("Thanh toán");
        }
        //danhsachsanbong
        public static void baotri()
        {
            MessageBox.Show("Bảo trì");
        }
        public static void baotrixong()
        {
            MessageBox.Show("Bảo trì xong");
        }
        public static void thaydoitrangthaisanthatbai()
        {
            MessageBox.Show("Sân đang được sử dụng");
        }
        //hoadon
        public static void thanhtoanthanhcong()
        {
            MessageBox.Show("Thanh toán thành công");
        }
        public static void thanhtoanthatbai()
        {
            MessageBox.Show("Thanh toán thất bại");
        }
        //phieudatsan
        public static void datsanthanhcong()
        {
            MessageBox.Show("Đặt sân thành công");
        }
        public static void datsanthatbai()
        {
            MessageBox.Show("Đặt sân thất bại");
        }
        //sanbong
        public static void saingay()
        {
            MessageBox.Show("Sai ngày");
        }
        public static void saithoigian()
        {
            MessageBox.Show("Sai thời gian");
        }
        public static void saigio()
        {
            MessageBox.Show("Sai giờ");
        }
        public static void khongthexoaquyenhan()
        {
            MessageBox.Show("Không thể xóa nhân viên có quyền quản lý.");
        }
        public static void nvdatontai()
        {
            MessageBox.Show("Nhân viên đã tồn tại");
        }
        public static void thanhcong()
        {
            MessageBox.Show("Thành công");
        }
        public static void thatbai()
        {
            MessageBox.Show("Thất bại");
        }
        public static void chuanhap()
        {
            MessageBox.Show("Chưa nhập");
        }
    }
}
