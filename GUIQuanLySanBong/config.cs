﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSB
{
    class config
    {
        public static string exit = "Bạn có muốn thoát không ? ";
        public static bool checkExistForm(string name, Form[] frms)
        {
            bool check = false;
            foreach (Form frm in frms)
            {
                if (frm.Name == name)
                {
                    check = true;
                    break;
                }
            }
            return check;
        }

        public static void activeChilForm(string name, Form[] frms)
        {
            foreach (Form frm in frms)
            {
                if (frm.Name == name)
                {
                    frm.Activate();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.StartPosition = FormStartPosition.CenterParent;
                    break;
                }
            }
        }
    }
}
