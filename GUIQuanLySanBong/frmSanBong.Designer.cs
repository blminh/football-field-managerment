﻿namespace QLSB
{
    partial class frmSanBong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnLamMoi = new DevExpress.XtraEditors.SimpleButton();
            this.dateFrom = new DevExpress.XtraEditors.DateEdit();
            this.timeTo = new DevExpress.XtraEditors.TimeEdit();
            this.timeFrom = new DevExpress.XtraEditors.TimeEdit();
            this.btnDat = new DevExpress.XtraEditors.SimpleButton();
            this.btnTim = new DevExpress.XtraEditors.SimpleButton();
            this.chkLoai7 = new DevExpress.XtraEditors.CheckEdit();
            this.chkLoai11 = new DevExpress.XtraEditors.CheckEdit();
            this.chkLoai5 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.flpDSSanBong = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLoai7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLoai11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLoai5.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnLamMoi);
            this.groupControl1.Controls.Add(this.dateFrom);
            this.groupControl1.Controls.Add(this.timeTo);
            this.groupControl1.Controls.Add(this.timeFrom);
            this.groupControl1.Controls.Add(this.btnDat);
            this.groupControl1.Controls.Add(this.btnTim);
            this.groupControl1.Controls.Add(this.chkLoai7);
            this.groupControl1.Controls.Add(this.chkLoai11);
            this.groupControl1.Controls.Add(this.chkLoai5);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(800, 80);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Đặt nhiều sân";
            // 
            // btnLamMoi
            // 
            this.btnLamMoi.Location = new System.Drawing.Point(629, 26);
            this.btnLamMoi.Name = "btnLamMoi";
            this.btnLamMoi.Size = new System.Drawing.Size(78, 48);
            this.btnLamMoi.TabIndex = 14;
            this.btnLamMoi.Text = "Làm mới";
            this.btnLamMoi.Click += new System.EventHandler(this.btnLamMoi_Click);
            // 
            // dateFrom
            // 
            this.dateFrom.EditValue = null;
            this.dateFrom.Location = new System.Drawing.Point(230, 27);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFrom.Size = new System.Drawing.Size(100, 20);
            this.dateFrom.TabIndex = 13;
            this.dateFrom.EditValueChanged += new System.EventHandler(this.dateFrom_EditValueChanged);
            // 
            // timeTo
            // 
            this.timeTo.EditValue = null;
            this.timeTo.Location = new System.Drawing.Point(388, 41);
            this.timeTo.Name = "timeTo";
            this.timeTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeTo.Properties.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.TouchUI;
            this.timeTo.Size = new System.Drawing.Size(100, 20);
            this.timeTo.TabIndex = 12;
            this.timeTo.EditValueChanged += new System.EventHandler(this.timeTo_EditValueChanged);
            // 
            // timeFrom
            // 
            this.timeFrom.EditValue = null;
            this.timeFrom.Location = new System.Drawing.Point(230, 53);
            this.timeFrom.Name = "timeFrom";
            this.timeFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeFrom.Properties.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.TouchUI;
            this.timeFrom.Size = new System.Drawing.Size(100, 20);
            this.timeFrom.TabIndex = 11;
            this.timeFrom.EditValueChanged += new System.EventHandler(this.timeFrom_EditValueChanged);
            // 
            // btnDat
            // 
            this.btnDat.Location = new System.Drawing.Point(710, 26);
            this.btnDat.Name = "btnDat";
            this.btnDat.Size = new System.Drawing.Size(78, 48);
            this.btnDat.TabIndex = 8;
            this.btnDat.Text = "Đặt sân";
            this.btnDat.Click += new System.EventHandler(this.btnDat_Click);
            // 
            // btnTim
            // 
            this.btnTim.Location = new System.Drawing.Point(545, 26);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(78, 48);
            this.btnTim.TabIndex = 7;
            this.btnTim.Text = "Tìm";
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // chkLoai7
            // 
            this.chkLoai7.Location = new System.Drawing.Point(72, 41);
            this.chkLoai7.Name = "chkLoai7";
            this.chkLoai7.Properties.Caption = "7 người";
            this.chkLoai7.Size = new System.Drawing.Size(57, 19);
            this.chkLoai7.TabIndex = 6;
            // 
            // chkLoai11
            // 
            this.chkLoai11.Location = new System.Drawing.Point(135, 41);
            this.chkLoai11.Name = "chkLoai11";
            this.chkLoai11.Properties.Caption = "11 người";
            this.chkLoai11.Size = new System.Drawing.Size(63, 19);
            this.chkLoai11.TabIndex = 5;
            // 
            // chkLoai5
            // 
            this.chkLoai5.Location = new System.Drawing.Point(13, 40);
            this.chkLoai5.Name = "chkLoai5";
            this.chkLoai5.Properties.Caption = "5 người";
            this.chkLoai5.Size = new System.Drawing.Size(60, 19);
            this.chkLoai5.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(351, 40);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(31, 18);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Đến:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(203, 40);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(21, 18);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Từ:";
            // 
            // flpDSSanBong
            // 
            this.flpDSSanBong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpDSSanBong.Location = new System.Drawing.Point(0, 86);
            this.flpDSSanBong.Name = "flpDSSanBong";
            this.flpDSSanBong.Size = new System.Drawing.Size(800, 365);
            this.flpDSSanBong.TabIndex = 2;
            // 
            // frmSanBong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.flpDSSanBong);
            this.Controls.Add(this.groupControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSanBong";
            this.Text = "Sân bóng";
            this.Load += new System.EventHandler(this.frmSanBong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLoai7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLoai11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLoai5.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit chkLoai7;
        private DevExpress.XtraEditors.CheckEdit chkLoai11;
        private DevExpress.XtraEditors.CheckEdit chkLoai5;
        private DevExpress.XtraEditors.SimpleButton btnTim;
        private DevExpress.XtraEditors.SimpleButton btnDat;
        private System.Windows.Forms.FlowLayoutPanel flpDSSanBong;
        private DevExpress.XtraEditors.TimeEdit timeFrom;
        private DevExpress.XtraEditors.TimeEdit timeTo;
        private DevExpress.XtraEditors.DateEdit dateFrom;
        private DevExpress.XtraEditors.SimpleButton btnLamMoi;
    }
}