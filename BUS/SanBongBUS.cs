﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
using System.Windows.Forms;

namespace BUS
{
    public class SanBongBUS
    {
        SanBongDAO sbDAO = new SanBongDAO();
        PhieuDatSanBUS pdsBUS = new PhieuDatSanBUS();

        public List<SanBongDTO> layDSSanBong()
        {
            return sbDAO.layDSSanBong();
        }

        public List<SanBongDTO> layDSSanBong(Int16 trangthai)
        {
            return layDSSanBong()
                .Where(u=>u.TrangThai == trangthai)
                .Select(s=>s).ToList();
        }

        public List<SanBongDTO> layDSSanBong(ThoiGian tg)
        {
            List<SanBongDTO> lst = layDSSanBong(1);
            List<string> lstMaSan = new List<string>();
            foreach (PhieuDatSanDTO pds in pdsBUS.dsDatSan())
            {
                int tgRa = tg.ThoiGianRa.Hour * 60 + tg.ThoiGianRa.Minute + 30;
                int tgVao = tg.ThoiGianVao.Hour * 60 + tg.ThoiGianVao.Minute - 30;
                int ngay = tg.ThoiGianVao.Day;

                if(pds.GioBatDau.Day == ngay)
                    if (pds.GioBatDau.Hour * 60 + pds.GioBatDau.Minute > tgRa)
                        continue;
                    else if (pds.GioKetThuc.Hour * 60 + pds.GioKetThuc.Minute < tgVao)
                        continue;
                    else
                    {
                        lstMaSan.Add(pds.MaSan);
                    }
            }

            if(lstMaSan != null)
                foreach (string item in lstMaSan)
                {
                    lst.RemoveAt(lst.FindIndex(x => x.MaSanBong == item));
                }
            
            return lst;
        }

        public List<SanBongDTO> layDSSanBongTheoLoai(byte loai)
        {
            return layDSSanBong()
                .Where(u => u.LoaiSanBong == loai)
                .Select(s => s).ToList();
        }

        public SanBongDTO laySanBongTheoMa(string ma)
        {
            return layDSSanBong()
                .Where(u=>u.MaSanBong == ma)
                .Select(s=>s).ToList()[0];
        }

        public List<string> dskhuvuc()
        {
            List<string> khuvuc = (from kv in sbDAO.layDSSanBong()
                                   group kv by kv.KhuVuc into dsKhuVuc
                                   orderby dsKhuVuc.Key
                                   select dsKhuVuc.Key).ToList();
            return khuvuc;
        }

        public string phatSinhMaSanBong(string kv, byte loai)
        {
            string ma = "";
            int sl = sbDAO.layDSSanBong()
                .Where(u=>u.KhuVuc == kv && u.LoaiSanBong == loai)
                .Count()+1;
            ma = kv+loai+"0"+sl;
            return ma;
        }

        public string phatSinhTenSanBong(string kv, byte loai)
        {
            string ten = "";
            int sl = sbDAO.layDSSanBong()
                .Where(u=>u.KhuVuc == kv && u.LoaiSanBong == loai)
                .Count()+1;
            ten = "san " + kv + loai + "-" + sl;
            return ten;
        }

        public bool ThemSanBong(SanBongDTO sbDTO)
        {
            sbDTO.MaSanBong = phatSinhMaSanBong(sbDTO.KhuVuc, sbDTO.LoaiSanBong);
            sbDTO.TenSanBong = phatSinhTenSanBong(sbDTO.KhuVuc, sbDTO.LoaiSanBong);
            return sbDAO.ThemSanBong(sbDTO);
        }

        public bool SuaSanBong(string masan)
        {
            foreach (PhieuDatSanDTO item in pdsBUS.dsDatSan())
            {
                if (item.MaSan == masan)
                    return false;
            }
            return sbDAO.SuaSanBong(masan);
        }

        public bool XoaSanBong(string masan)
        {
            foreach (PhieuDatSanDTO item in pdsBUS.dsDatSan())
            {
                if (item.MaSan == masan)
                    return false;
            }
            return sbDAO.XoaSanBong(masan);
        }
    }
}
