﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;

namespace BUS
{
    public class PhieuDatSanBUS
    {
        PhieuDatSanDAO pdsDAO = new PhieuDatSanDAO();
        public List<PhieuDatSanDTO> dsDatSan()
        {
            return pdsDAO.dsDatSan();
        }

        public List<PhieuDatSanDTO> dsPhieuDatSan(string sdt, DateTime dt)
        {
            return pdsDAO.dsPhieuDatSan(sdt, dt);
        }

        public bool ThemPhieuDatSan(List<PhieuDatSanDTO> lst)
        {
            return pdsDAO.themPhieu(lst);
        }

        public void ThanhToanPhieu(PhieuDatSanDTO pdsDTO)
        {
            pdsDAO.ThanhToanPhieu(pdsDTO);
        }

        public void HuyPhieu(PhieuDatSanDTO pdsDTO)
        {
            pdsDAO.HuyPhieu(pdsDTO);
        }
    }
}
