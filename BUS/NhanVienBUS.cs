﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DevExpress.XtraGrid;
using DTO;
namespace BUS
{
    public class NhanVienBUS
    {
        NhanVienDAO nvDAO = new NhanVienDAO();
        public void loadDSNhanVien(GridControl gvDS)
        {
            gvDS.DataSource = nvDAO.loadDSNhanVien();
        }
        public List<NhanVienDTO> loadDSNhanVien()
        {
            return nvDAO.loadDSNhanVien();
        }
        public List<NhanVienDTO> layDSTaiKhoan()
        {
            return nvDAO.layDSTaiKhoan();
        }
        public NhanVienDTO timNhanVien(string maNV)
        {
            return nvDAO.Timnv(maNV);
        }
        public string MaHoa(string pass)
        {
            return nvDAO.MaHoa(pass);
        }
        public bool themNhanVien(NhanVienDTO nvDTO)
        {
            return nvDAO.themNhanVien(nvDTO);
        }
        public bool suaNhanVien(NhanVienDTO nvDTO)
        {
            return nvDAO.suaNhanVien(nvDTO);
        }
        public bool xoaNhanVien(string maNV)
        {
            return nvDAO.xoaNhanVien(maNV);
        }
    }
}
