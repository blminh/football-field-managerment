﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;

namespace BUS
{
    public class BangGiaBUS
    {
        SanBongDAO sbdao = new SanBongDAO();
        BangGiaDAO bgDAO = new BangGiaDAO();
        public List<BangGiaDTO> layDSBangGia()
        {
            return bgDAO.layDSBangGia();
        }
        public List<BangGiaDTO> LayDSLoaiSanBong(byte loaisb)
        {
            return bgDAO.layDSSanBongTheoLoai(loaisb);


        }
    }
}
