﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;

namespace BUS
{

    public class DichVuBUS
    {
        DichVuDAO dichvu = new DichVuDAO();
        public List<DichVuDTO> layDSDV()
        {
            return dichvu.dsChiTiet();
        }
        public List<DichVuDTO> layDSDV1(string maloai)
        {
            return dichvu.dsChiTiet1(maloai);
        }
        

    }

}