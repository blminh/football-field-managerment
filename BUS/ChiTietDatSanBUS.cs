﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;

namespace BUS
{
    public class ChiTietDatSanBUS
    {
        ChiTietDatSanDAO ctDAO = new ChiTietDatSanDAO();
        public List<ChiTietDatSanDTO> dsChiTiet()
        {
            return ctDAO.dsChiTiet();
        }
        public bool ThemChiTietDatSan(ChiTietDatSanDTO ctdsDTO)
        {
            return ctDAO.ThemChiTietHoaDon(ctdsDTO);
        }
    }
} 
