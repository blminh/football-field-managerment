﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DevExpress.XtraGrid;
using DTO;

namespace BUS
{
    public class LoaiNVBUS
    {
        LoaiNVDAO lnvDAO = new LoaiNVDAO();
        public List<LoaiNVDTO> layDSLoaiNV()
        {
            return lnvDAO.layDSLoaiNV();
        }
    }
}
