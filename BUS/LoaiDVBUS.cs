﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;

namespace BUS
{
    public class LoaiDVBUS
    {
        LoaiDVDAO bgDAO = new LoaiDVDAO();
        public List<LoaiDVDTO> layDSLoaiDV()
        {
            return bgDAO.layDSloaiDV();
        }
        
    }
}
