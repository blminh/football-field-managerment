﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DevExpress.XtraGrid;
using DTO;

namespace BUS
{
    public class KhachHangBUS
    {
        KhachHangDAO khDAO = new KhachHangDAO();

        public List<KhachHangDTO> loadDSKhachHang()
        {
            return khDAO.loadDSKhachHang();
        }

        public bool KiemTraKhachThanhVien(string sdt)
        {
            foreach (KhachHangDTO item in loadDSKhachHang())
            {
                if (item.SoDienThoai == sdt)
                    return true;
            }
            return false;
        }

        public void ThangHangKhachHang(decimal giatrihd, string sdt)
        {   
            KhachHangDTO khDTO = LayKhachHangTheoSDT(sdt);
            if (khDTO.MaKH == "KH00")
                return;
            double diem = Convert.ToDouble(giatrihd)/1000*0.01 + khDTO.DIEMTICHLUY;
            if (diem < 10)
                khDTO.LoaiKH = 0;
            else if (diem < 50)
                khDTO.LoaiKH = 1;
            else if (diem < 100)
                khDTO.LoaiKH = 2;
            else if (diem < 150)
                khDTO.LoaiKH = 3;
            else khDTO.LoaiKH = 4;
            khDTO.DIEMTICHLUY = diem;
            khDAO.ThangHangKhachHang(khDTO);
        }

        public int KiemTraLoaiKhachHang(string sdt)
        {
            int loai = 0;
            foreach (KhachHangDTO item in loadDSKhachHang())
            {
                if (item.SoDienThoai == sdt)
                    loai = item.LoaiKH;
            }
            return loai;
        }

        public KhachHangDTO LayKhachHangTheoSDT(string sdt)
        {
            foreach (KhachHangDTO item in loadDSKhachHang())
            {
                if (item.SoDienThoai == sdt)
                    return item;
            }
            return loadDSKhachHang()[0];
        }

        public bool ThemKhachThanhVien(KhachHangDTO khDTO)
        {
            return khDAO.ThemKhachHang(khDTO);
        }
        public bool SuaThongTinKH(KhachHangDTO suakhDTO)
        {
            return khDAO.SuaKhachHang(suakhDTO);
        }
        public KhachHangDTO TimKiemThongTinKH(string maKh)
        {
            return khDAO.TimKh(maKh); 
        }
        public bool XoaThongTinKH(string makh)
        {
            return khDAO.XoaKhachHang(makh);
        }
    }
}
