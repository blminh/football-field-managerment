﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;

namespace BUS
{
    public class HoaDonBUS
    {
        HoaDonDAO hdDAO = new HoaDonDAO();
        public List<HoaDonDTO> layDSHoaDon()
        {
            return hdDAO.layDSHoaDon();
        }
        public bool ThemHoaDon(HoaDonDTO hdDTO)
        {
            return hdDAO.ThemHoaDon(hdDTO);
        }
    }
}
